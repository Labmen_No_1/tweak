import os
import sys
import json
import zipfile
import requests as http
from dotenv import load_dotenv

load_dotenv()

ROOT = os.getenv('PACK_ROOT')
KEY = os.getenv('KEY')
WX_BOT_URL = 'https://qyapi.weixin.qq.com/cgi-bin/webhook'
WEB_HOOK = f'{WX_BOT_URL}/send?key={KEY}'
UPLOAD_API = f'{WX_BOT_URL}/upload_media?type=file&key={KEY}'


def pack(sha1_code: str) -> str:
    """
    打包 java 代码
    :param sha1_code: git 提交的 sha1 值
    :return: zip 文件路径 str
    """
    file_name = f'tweak_{sha1_code}.zip'
    with zipfile.ZipFile(file_name, 'w', zipfile.ZIP_DEFLATED) as zf:
        for root, _, files in os.walk(ROOT):
            for file in files:
                full_path = os.path.join(root, file)
                # 去掉冗余的前缀路径
                arc_path = full_path.replace(ROOT, '')
                zf.write(full_path, arc_path)
    return file_name


def upload(file_path: str) -> str | None:
    """
    文件上传
    :return:
    """
    with open(file_path, 'rb') as f:
        with http.post(UPLOAD_API, files={'media': f}) as resp:
            if resp and resp.status_code == 200:
                info = json.loads(resp.text)
                print(info)
                return info.get('media_id', None)


def publish(media_id: str, zip_file: str):
    """
    :param zip_file:
    :param media_id:
    :return:
    """
    with http.post(WEB_HOOK, json={'msgtype': 'file', 'file': {'media_id': media_id}}) as resp:
        if resp and resp.status_code == 200:
            info = json.loads(resp.text)
            print(info)
            code, msg = int(info.get('errcode', -1)), str(info.get('errmsg', '')).strip()
            if code == 0 and msg == 'ok':
                clean(zip_file)
                print('publish success')
                return
        print('publish failed', file=sys.stderr)


def send_msg(content: str):
    """
    send text msg
    :param content:
    :return:
    """
    with http.post(WEB_HOOK, json={'msgtype': 'text', 'text': {'content': content}}) as resp:
        if resp and resp.status_code == 200:
            print('send msg success')


def clean(zip_file: str):
    if os.path.isfile(zip_file):
        os.remove(zip_file)


def main():
    params = sys.argv
    sha1_code = params[1]
    zip_file = pack(sha1_code)
    media_id = upload(zip_file)
    if not media_id:
        print('upload failed', file=sys.stderr)
        sys.exit(-1)
    publish(media_id, zip_file)
    if len(params) > 2:
        content = params[2]
        if not content == 'code':
            send_msg(content)


if __name__ == '__main__':
    main()
