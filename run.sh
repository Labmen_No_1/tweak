#!/bin/bash

commit_content="code"

if [ "$1" = "diy" ]; then
    read -rp "Enter commit content " commit_content
fi

echo "$commit_content"

git add . && git commit -m "$commit_content"
SHA1=$(git rev-parse --short HEAD)
echo "$SHA1"
# git log -n 1 --format=%B
python -u publisher.py "$SHA1" "$commit_content"
exit 0