#!/bin/bash

set -xe
echo -e "show points in matlib plot"
# get new generate json file name without extend name
# if you want to reverse the oldest file use ls -ltr use r to reverse
param=$(ls -lt dump/*.json | head -n 1 | perl -pe 's/^.+?dump\/(.+?)\.json$/$1/g')
echo -e "file name is $param"
# C colorful c one line
cat dump/${param}.json | jq 'keys' -c
python -u show_points.py "$param"
exit 0