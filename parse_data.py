import toml
import json
import sys


def make_point(xyz: list[float]):
    return {
        'x': xyz[0],
        'y': xyz[1],
        'z': xyz[2],
    }


def transform(key):
    # python regex not support /U /L uppercase lowercase replace
    return str(key).replace('_a', 'A').replace('_b', 'B').replace('_s', 'S') if '_' in key else key


def main():
    file_name = sys.argv[1]

    obj = dict()

    with open(f'dump/{file_name}.toml', 'r', encoding='utf-8') as f:
        data = toml.load(f)
        for k, v in data.items():
            if k == 'detail': continue
            if k == 'angle':
                for k0, v0 in v.items():
                    new_key = transform(k0) + 'Angle'
                    obj[new_key] = v0
            else:
                new_key = transform(k)
                print(new_key)
                points = []
                for k1, v1 in v.items():
                    print(k1, v1)
                    if len(points) == 3:
                        break
                    points.append(make_point(v1))
                obj[new_key] = points
    with open(f'data/{file_name}.json', 'w') as f:
        json.dump(obj, f)


if __name__ == '__main__':
    main()
