#!/bin/bash

read -rp "Enter file name: " file_name

file_path="dump/${file_name}.toml"

if [[ -e "$file_path"  ]]; then
    echo "$file_path exists."
    python -u parse_data.py "$file_name"
    exit 0
else
    echo "$file_path does not exist."
    exit 1
fi