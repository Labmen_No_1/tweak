import json
import sys

from matplotlib import pyplot as plt

color_dict = {
    'm': 'r',
    'mb': 'orange',
    'mc': 'y',
    'md': 'g',
    'me': 'c',
    'proj': 'b',
    't': 'b',
}


def square(axis, data: list[dict], key_name: str):
    x = [elm['x'] for elm in data]
    y = [elm['y'] for elm in data]
    z = [elm['z'] for elm in data]

    color = color_dict.get(key_name, 'r')

    axis.scatter(x, y, z, c=color, marker='o')
    if key_name == 'm':
        for idx, elm in enumerate(zip(x, y, z)):
            axis.text(*elm, f'{idx+1}', color='b')

    for i in (0, 3):
        for j in (1, 2):
            axis.plot([x[i], x[j]], [y[i], y[j]], [z[i], z[j]], c=color)


def main():
    f_name = sys.argv[1]
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    with open(f'dump/{f_name}.json', 'r', encoding='utf-8') as f:
        json_data = json.load(f)
        for k, v in json_data.items():
            square(ax, v, k)
    plt.show()


if __name__ == '__main__':
    main()
