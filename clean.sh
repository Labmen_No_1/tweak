#!/bin/bash

read -rp "Enter file name: " file_name

out_path="dump/${file_name}.toml"
log_path="info.log"

clear() {
  python -u clean_data.py "$1" "$2"
}

if [[ -e "$out_path" ]]; then
  echo "" > "$out_path"
fi

# get measure angles
angles=$(grep "当前角度" "$log_path" | head -n 4 | perl -pe 's/^.+?当前角度:([-\.\d]+),([-\.\d]+)$/$2,$1/gm;s/\n/;/g')
echo $(clear "angle" "$angles") >> "$out_path"

# tiktok points declare -a
arr=("VerticalBefore" "VerticalAfter" "HorizontalBefore" "HorizontalAfter" "LatitudeBefore" "LatitudeAfter" "LongitudeBefore" "LongitudeAfter")
for elm in "${arr[@]}"; do
  points=$(grep -A 3 "$elm" "$log_path"  | perl -pe 's/^.+?x=([-\.\d]+), y=([-\.\d]+), z=([-\.\d]+).+?$/$1,$2,$3/g'| sed -n '2,4p' | perl -pe 's/\n/;/g')
  echo $(clear "$elm" "$points") >> "$out_path"
done

# measure points
measure=$(grep -A 3 "Fit" "$log_path" | sed -n '2,4p' | perl -pe 's/^.+?x=([-\.\d]+), y=([-\.\d]+), z=([-\.\d]+).+?$/$1,$2,$3/g;s/\n/;/g')
echo $(clear "measure" "$measure") >> "$out_path"

# target points
echo $(clear "target" "") >> "$out_path"

exit 0