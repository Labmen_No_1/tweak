import sys

map = {
    "LatitudeBefore": "short_before",
    "LatitudeAfter": "short_after",
    "LongitudeBefore": "long_before",
    "LongitudeAfter": "long_after",
    "VerticalBefore": "v_shift_before",
    "VerticalAfter": "v_shift_after",
    "HorizontalBefore": "h_shift_before",
    "HorizontalAfter": "h_shift_after",
    "measure": "measure",
}


def fmt_angle(content: str):
    arr = content.split(';')
    return """
[angle]
short_before = [{}]
short_after = [{}]
long_before = [{}]
long_after = [{}]
    """.format(*arr)


def fmt_points(content: str, flag):
    arr = content.split(';')
    return """
[{}]
p1 = [{}]
p2 = [{}]
p3 = [{}]
    """.format(flag, *arr)


def main():
    params = sys.argv
    flag = params[1]
    content = params[2]
    std_out = ''
    if flag == 'angle':
        std_out = fmt_angle(content)
    elif flag == 'target':
        std_out = fmt_points('6.0116,2.0089,-0.6803;4.1552,1.7898,-0.6580;6.4991,-2.1283,-0.6819', flag)
    else:
        key = map.get(str(flag), '')
        std_out = fmt_points(content, key)
    print(std_out)


if __name__ == '__main__':
    main()
