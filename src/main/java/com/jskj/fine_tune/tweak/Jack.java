package com.jskj.fine_tune.tweak;

/**
 * 千斤顶
 */
public class Jack {
    /**
     * 千斤顶编号
     * 0 base
     */
    int num = 0;

    /**
     * 千斤顶类型
     */
    JackType jackType;

    /**
     * 当前前景顶的行程
     */
    double curDistance;

    /**
     * 千斤顶长度
     * VERTICAL 类型的不需要设置
     */
    double length;

    /**
     * 最大行程
     */
    double maxDistance;

    /**
     * 千斤顶相对于车的安装位置
     * 对于类型为 VERTICAL 的计算千斤顶圆心到原点的距离
     * 对于类型为 ROTATE、HORIZON 的计算千斤顶固定点到原点的距离
     */
    double posX, posY;

    /**
     * 电缸累计偏转角度
     */
    double degrees = 0.;


    public String getJackName() {
        return String.format("%s%d", jackType.alias, num);
    }

    public Jack(JackType jackType, double curDistance, double length, double maxDistance, double posX, double posY) {
        this.jackType = jackType;
        this.curDistance = curDistance;
        this.length = length;
        this.maxDistance = maxDistance;
        this.posX = posX;
        this.posY = posY;
    }

    /**
     * 计算千斤顶的总长度
     * 千斤顶的当前行程+千斤顶的长度
     *
     * @return float64
     */
    public double getTotalLength() {
        return (this.curDistance + this.length);
    }

    /**
     * 获取千斤顶的安装位置坐标（相对于车）
     * @return AxisInfo obj
     */
    public AxisInfo getJackRelativePoint() {
        return new AxisInfo(this.posX, this.posY, 0);
    }




    public Jack(int num, JackType type, double max, double len, double curVal) {
        this.num = num;
        this.jackType = type;
        this.maxDistance = max;
        this.length = len;
        this.curDistance = curVal;
    }

    public Jack(int num, JackType type, double max, double curVal) {
        this.num = num;
        this.jackType = type;
        this.maxDistance = max;
        this.curDistance = curVal;
    }

    public Jack() {
    }

    public double getCurDistance() {
        return curDistance;
    }

    public void setCurDistance(double curDistance) {
        this.curDistance = curDistance;
    }

    public boolean check(double val) {
        double res = curDistance + val;
        return res > maxDistance || res < 0;
    }

    public double calcShiftVal(double val) {
        double res = curDistance + val;
        if (res > maxDistance) return (maxDistance - res);
        else if (res < 0) return res;
        else return 0;
    }

    public double getMaxDistance() {
        return maxDistance;
    }
}
