package com.jskj.fine_tune.tweak;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface Group {

    /**
     * 补齐第四个点
     *
     * @param points three points
     * @return List[AxisInfo]
     */
    default List<AxisInfo> padPoint(AxisInfo... points) {
        List<AxisInfo> ret = new ArrayList<>(Arrays.asList(points));
        ret.add(M.get4thPoint(points));
        return ret;
    }
}
