package com.jskj.fine_tune.tweak;

/**
 * 精调车的承载板
 */
public class VehicleBoard {
    double length, width, height, installX, installY;

    public VehicleBoard(double length, double width, double height, double installX, double installY) {
        this.length = length;
        this.width = width;
        this.height = height;
        this.installX = installX;
        this.installY = installY;
    }

    static VehicleBoard getDefault() {
        return new VehicleBoard(
                G.V_BOARD_LENGTH,
                G.V_BOARD_WIDTH,
                G.V_BOARD_HEIGHT,
                G.V_BOARD_INSTALL_X,
                G.V_BOARD_INSTALL_Y
        );
    }
}
