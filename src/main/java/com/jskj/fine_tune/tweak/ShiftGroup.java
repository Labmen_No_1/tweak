package com.jskj.fine_tune.tweak;

import java.util.List;

import static com.jskj.fine_tune.tweak.M.*;

/**
 * 平移测量组
 */
public class ShiftGroup implements Group {
    enum Direction {
        VERTICAL, HORIZON
    }

    List<AxisInfo> beforePoints;

    List<AxisInfo> afterPoints;

    Direction direction;

    double talRefX, totalRefY;

    ShiftGroup(Direction direction) {
        this.direction = direction;
    }

    void setBeforePoints(AxisInfo... points) {
        beforePoints = padPoint(points);
    }

    void setAfterPoints(AxisInfo... points) {
        afterPoints = padPoint(points);
    }

    /**
     * 计算偏移角
     *
     * @return double
     */
    double calcShiftAngle() {
        boolean cond = direction == Direction.VERTICAL;
        AxisInfo b1 = beforePoints.get(0),
                 b2 = beforePoints.get(1),
                 b3 = beforePoints.get(2),
                 b4 = beforePoints.get(3);

        AxisInfo a1 = afterPoints.get(0),
                 a2 = afterPoints.get(1);

        AxisInfo midB12 = mid(b1, b2),
                 midA12 = mid(a1, a2);

        AxisInfo vec1 = sub(midB12, midA12);
        // VERTICAL 12mid -> 34mid
        // HORIZON  13mid -> 24mid
        AxisInfo vec2 = cond ? sub(midB12, mid(b3, b4)) : sub( mid(b2, b4), mid(b1, b3));

        return calcVecAngle0(vec1, vec2);
    }

}
