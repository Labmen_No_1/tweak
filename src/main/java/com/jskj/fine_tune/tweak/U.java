package com.jskj.fine_tune.tweak;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * 通用工具
 */
public final class U {
    public static final String TAG = "[TWEAK]";
    public static final String DUMP_TAG = "[TWEAK_DUMP]";

    /**
     * 打印信息
     *
     * @param msg 调试信息
     */
    public static void print(String msg, boolean dump) {
        if (G.DUB_MOD) System.out.println(dump ? DUMP_TAG : TAG + msg);
    }

    public static void print(String fmt, Object... params) {
        print(f(fmt, params));
    }

    /**
     * 打印多个点
     * @param points
     */
    public static void dumpPoints(AxisInfo... points) {
        Stream.of(points).forEach(elm -> print(elm.toString()));
        System.out.println();
    }

    /**
     * dump 为 java 代码用于测试
     *
     * @param tag 变量前缀
     * @param points 坐标点
     */
    public static void dumpPoints2Code(String tag, AxisInfo... points) {
        for (int i = 0, len = points.length; i < len; ++i) {
            int num = (i + 1);
            AxisInfo point = points[i];
            print(f("AxisInfo %s%d = %s;", tag, num, point.toString()));
        }
        System.out.println();
    }

    public static void print(String msg) {
        print(msg, false);
    }

    /**
     * 格式化信息
     * @param fmt
     * @param params
     * @return
     */
    public static String f(String fmt, Object... params) {
        return String.format(fmt, params);
    }

    public static String symbol(double num) {
        if (num == 0) return "0";
        return (num > 0 ? "+" : "-") + "1";
    }

    public static List<Double[]> zip(double[] a, double[] b) {
        int len = a.length;
        ArrayList<Double[]> ret = new ArrayList<>(len);
        for (int i = 0; i < len; ++i) {
            ret.add(new Double[]{a[i], b[i]});
        }
        return ret;
    }

}
