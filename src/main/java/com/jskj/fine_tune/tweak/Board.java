package com.jskj.fine_tune.tweak;

public class Board {
    /**
     * 板件的长宽高
     */
    double length, width, height, laserWidth, laserLength;

    public Board(double length, double width, double height, double laserWidth, double laserLength) {
        this.length = length;
        this.width = width;
        this.height = height;
        this.laserWidth = laserWidth;
        this.laserLength = laserLength;
    }

    /**
     * 获取一个默认值
     *
     * @return Board
     */
    public static Board getDefault() {
        return new Board(G.BOARD_LENGTH, G.BOARD_WIDTH, G.BOARD_HEIGHT, 1514, 1785);
    }
}
