package com.jskj.fine_tune.tweak;

import java.util.ArrayList;
import java.util.List;

import static com.jskj.fine_tune.tweak.G.*;
import static com.jskj.fine_tune.tweak.M.abs;
import static com.jskj.fine_tune.tweak.M.sub;
import static com.jskj.fine_tune.tweak.U.*;

public class MeasureGroup extends PointGroup {

    /**
     * 板表面的坐标
     */
    public List<AxisInfo> bList;

    /**
     * 千斤顶 D 坐标
     * 一旦确定不会改变
     */
    public List<AxisInfo> dList;

    public double height;

    public MeasureGroup(Slope slope, AxisInfo... points) {
        super(slope, points);
        height = slope.calcHeight();
    }

    /**
     * 更新测量坐标
     * @param points
     */
    public void updateMeasurePoints(AxisInfo... points) {
        setParams(points);
    }

    @Override
    protected void calcPointC() {
        double h = height + BOARD_HEIGHT;
        cList = commonStep(h);
    }

    /**
     * 计算 千斤顶在托盘面上的坐标
     */
    public void calcPointD(List<Double[]> jackRefValList) {
        List<RefPoint> refList = new ArrayList<>();
        for (int i = 0, len = cList.size(); i < len; ++i) {
            AxisInfo c = cList.get(i);
            Double[] refArr = jackRefValList.get(i);
            double refX = refArr[0], refY = refArr[1];
            RefPoint refPoint = new RefPoint(c, refX, refY);
            refList.add(refPoint);
        }
        dList = calcPointByRef(refList, V_JACK_SHORT_DIS, V_JACK_LONG_DIS);
    }

    public void calcPointB() {
        bList = commonStep(height);
    }

    public boolean check(List<AxisInfo> targetPoints, double tol) {
        for (int i = 0; i < points.size(); ++i) {
            AxisInfo m = points.get(i);
            AxisInfo t = targetPoints.get(i);
            AxisInfo res = sub(t, m);
            boolean bx = abs(res.x) > tol;
            boolean by = abs(res.y) > tol;
            boolean bz = abs(res.z) > tol;

            if (bx || by || bz) return false;
        }
        return true;
    }

    /**
     * 获取千斤顶 13 组成的
     * @return
     */
    public AxisInfo getJackVec(int from, int to) {
        AxisInfo fromP = dList.get(from);
        AxisInfo toP = dList.get(to);
        return sub(fromP, toP);
    }

    @Override
    public void chain(List<Double[]> refList) {
        // 1. B C
        calcPointB();
        calcPointC();
        if (dList == null) {
            calcPointD(refList);
            print("init d list done");
        }
        calcPointE(refList);
    }

    public int[] locateSelf() {
        int[] ret = new int[2];
        AxisInfo
                p1 = points.get(0),
                p2 = points.get(1),
                p3 = points.get(2);

        AxisInfo res1 = sub(p1, p2), res2 = sub(p1, p3);

        String str = f("%s%s%s%s",
                symbol(res1.x), symbol(res1.y), symbol(res2.x), symbol(res2.y));
        int a = 0, b = 0;
        switch (str) {
            case "-100+1":
            case "-1-1-1+1":
            case "-1+1+1+1":
                a = -1;
                b = -1;
                break;
            case "0-1-10":
            case "+1-1-1-1":
                a = -1;
                b = 1;
                break;
            case "+100-1":
            case "+1+1+1-1":
                a = 1;
                b = 1;
                break;
            case "0+1+10":
                a = 1;
                b = -1;
        }
        ret[0] = a; ret[1] = b;
        return ret;
    }
}
