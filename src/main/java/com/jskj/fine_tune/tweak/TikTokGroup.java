package com.jskj.fine_tune.tweak;

import java.util.ArrayList;
import java.util.List;

import static com.jskj.fine_tune.tweak.M.abs;
import static com.jskj.fine_tune.tweak.M.toDeg;
import static com.jskj.fine_tune.tweak.U.print;
import static com.jskj.fine_tune.tweak.U.symbol;

/**
 * 带有测斜仪的测量组
 */
public class TikTokGroup implements Group {
    /**
     * 抬升 长/短 边
     */
    private Side side;

    /**
     * 抬升前的测量值
     */
    private List<AxisInfo> beforePoints;

    /**
     * 抬升后的测量值
     */
    private List<AxisInfo> afterPoints;

    /**
     * 抬升前测斜仪测量的数值
     */
    private Pair beforeVal;

    /**
     * 抬升后测斜仪测量的数值
     */
    private Pair afterVal;

    /**
     * 存储 z 轴的变化量
     */
    private List<Double> zVals;

    private double angleX, angleY;

    static class Pair {
        double xVal;
        double yVal;

        public Pair(double xVal, double yVal) {
            this.xVal = conv(xVal);
            this.yVal = conv(yVal);
        }

        /**
         * 将测斜仪的测量数值转化为安全的锐角弧度值。
         *
         * @return
         */
        private double conv(double val) {
            return val;
        }

        @Override
        public String toString() {
            return "Pair{" +
                    "xVal=" + U.f("%.5f", xVal) +
                    ", yVal=" + U.f("%.5f", yVal) +
                    '}';
        }
    }

    public TikTokGroup(Side side) {
        this.side = side;
    }

    public void setBefore(double xVal, double yVal, AxisInfo... points) {
        beforeVal = new Pair(xVal, yVal);
        beforePoints = padPoint(points);
    }

    public void setAfter(double xVal, double yVal, AxisInfo... points) {
        afterVal = new Pair(xVal, yVal);
        afterPoints = padPoint(points);
        // 修正 z 值
        fixZVal();
        // 计算z轴差量
        getDeltaZ();
        // 根据坐标计算角度
        calcDeltaAngle();
    }

    /**
     * 计算 ref 的数值，绝对值
     *
     * @param delta z 轴的偏差量有正负
     * @return double
     */
    private double calcRefVal(double delta, int idx) {
        boolean cond = this.side == Side.LONG;

        double subVal = cond ?
                G.V_JACK_SHORT_DIS :
                G.V_JACK_LONG_DIS;

        double degDelta = cond ? angleX : angleY;
        double val = abs(delta / Math.sin(Math.toRadians(degDelta)));
        // 如果短或者长边抬起时，两个z变化量都是+，val≥subVal?(val-subVal):-val
        // print("num %d outer %b delta %.2f cond %s", (idx + 1), outer(cond, idx, delta), delta, side);
        return outer(cond, idx, delta) ?
                (val >= subVal ? (val - subVal) : -val) :
                (delta > 0 ? val - subVal : val);
    }

    public double[] getRefVal() {
        int len = this.zVals.size();
        double[] arr = new double[len];

        for (int i = 0; i < len; ++i) {
            double delta = zVals.get(i);
            print("%s %d delta z: %.2f", side, (i + 1), delta);
            arr[i] = calcRefVal(delta, i);
        }
        return arr;
    }

    /**
     * 计算 z 轴的变化量
     * short 13 24
     * long 12 34
     */
    private void getDeltaZ() {
        List<Double> list = new ArrayList<>();
        final int len = beforePoints.size();
        for (int i = 0; i < len; ++i) list.add(deltaZ(i));
        zVals = list;
    }

    private boolean outer(boolean cond, int idx, double delta) {
        boolean ret = false;
        String symbol = symbol(delta).substring(0, 1);
        if (cond) {
            switch (idx) {
                // up
                case 0:
                case 2:
                    ret = !"+".equals(symbol);
                    break;
                case 1:
                case 3:
                    ret = !"-".equals(symbol);
            }
        } else {
            switch (idx) {
                case 2:
                case 3:
                    ret = !"+".equals(symbol);
                    break;
                case 0:
                case 1:
                    ret = !"-".equals(symbol);
            }
        }
        return ret;
    }

    private double deltaZ(int idx) {
        return afterPoints.get(idx).z - beforePoints.get(idx).z;
    }

    /**
     * 计算角度变化
     * @return
     */
    private void calcDeltaAngle() {
        boolean cond = this.side == Side.LONG;

        double xDelta = afterVal.xVal - beforeVal.xVal;
        double yDelta = afterVal.yVal - beforeVal.yVal;
        print("equip side %s angleX %.5f angleY %.5f", side, xDelta, yDelta);

        double a = (cond ? G.LASER_WIDTH : G.LASER_LENGTH) - G.EPS;
        int num1 = cond ? 3 : 0;
        int num2 = cond ? 0 : 3;

        double deltaThree = deltaZ(2);
        double b = deltaZ(num1) - deltaThree;
        double c = deltaZ(num2) - deltaThree;

        double angle1 = toDeg(Math.asin(b / a));
        double angle2 = toDeg(Math.asin(c / a));
        this.angleX = cond ? angle1 : angle2;
        this.angleY = cond ? angle2 : angle1;
        print("side %s angleX %.5f angleY %.5f", side, angleX, angleY);
    }

     /**
     * 修正 z 的值
     */
    private void fixZVal() {
        boolean cond = side == Side.LONG;
        int num1 = cond ? 2 : 0;
        int num2 = cond ? 3 : 2;
        double val = cond ? G.LASER_WIDTH : G.LASER_LENGTH;

        AxisInfo p1 = afterPoints.get(num1);
        AxisInfo p2 = afterPoints.get(num2);

        double absDeg = abs(cond ? angleY : angleX);
        double v = val * Math.sin(Math.toRadians(absDeg));
        boolean subCond = cond ? (angleY <= 0) : (angleX <= 0);
        p1.z += subCond ? (-v) : v;
        p2.z += subCond ? (-v) : v;
    }
}
