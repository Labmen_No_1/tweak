package com.jskj.fine_tune.tweak;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class JackBrain {
    Map<String, Double> oldValMap;

    Map<String, Jack> currentMap;


    public JackBrain(Map<String, Double> params) {
        currentMap = new HashMap<>(8, 1);
        params.forEach((k, v) -> {

            char prefix = k.substring(0, 1).toUpperCase().toCharArray()[0];
            int num = Integer.parseInt(k.substring(1, 2));

            Jack jack;
            switch (prefix) {
                case 'V':
                    jack = new Jack(num, JackType.VERTICAL, 150, v);
                    break;
                case 'R':
                case 'H':
                    jack = new Jack(num,
                            (prefix == 'R' ? JackType.ROTATE : JackType.HORIZON),
                            200, 100, v);
                    break;
                default:
                    jack = new Jack();

            }
            currentMap.put(k, jack);
        });
        oldValMap = params;
    }

    /**
     * 更新千斤顶行程
     * @param params map
     */
    public void updateJackDis(Map<String, Double> params) {
        currentMap.forEach((k, v) -> {
            Double curVal = params.get(k);
            v.setCurDistance(curVal);
        });
        oldValMap = params;
    }

    public double alert(String key, double val) {
        Jack jack = currentMap.get(key);
        boolean check = jack.check(val);
        if (check) return jack.calcShiftVal(val);
        return 0d;
    }

    /**
     * 修改 currentMap 的行程
     * @param key 千斤顶简称代号
     * @param val 该变量 有符号
     */
    public void modify(String key, double val) {
        Jack jack = currentMap.get(key);
        double old = jack.getCurDistance();
        jack.setCurDistance(old + val);
    }

    public void setCurDis(String key, double curDis) {
        Jack jack = currentMap.get(key);
        jack.setCurDistance(curDis);
    }

    public double getVal(String key) {
        return currentMap.get(key).getCurDistance();
    }

    private Map<String, Double> toMap() {
        HashMap<String, Double> retMap = new HashMap<>(8, 1);
        for (Map.Entry<String, Jack> entry : currentMap.entrySet()) {
            String key = entry.getKey();
            double curVal = entry.getValue().getCurDistance();
            retMap.put(key, curVal);
        }
        return retMap;
    }

    public void check(TweakResult resp) {
        double hVal = 0, vVal = 0;
        for (Map.Entry<String, Jack> entry : currentMap.entrySet()) {
            String prefix = entry.getKey().substring(0, 1);
            Jack jack = entry.getValue();
            double cur = jack.getCurDistance();
            double max = jack.getMaxDistance();

            double out = (cur - max);
            double in = (0 - cur);

            boolean outCond = out > 0;
            boolean inCond = in > 0;
            // 有问题
            if (outCond || inCond) {
                switch (prefix) {
                    case "H":
                        hVal = Math.max(out, in);
                        break;
                    case "R":
                        vVal = Math.max(out, in);
                        break;
                    case "V":
                        U.print("do nothing about V jack");
                }
            }
        }/*loop*/
        if (hVal != 0 || vVal != 0) {
            resp.setCode(400);
            resp.setHorizonShift(hVal);
            resp.setVerticalShift(vVal);
        } else {
            resp.setCode(200);
            resp.setJackTweakMap(toMap());
        }
    }



}
