package com.jskj.fine_tune.tweak;

public interface G {

    boolean DUB_MOD = true;

    double EPS = 0.05;
    /**
     * 车的三维
     */
    double
            VEHICLE_LENGTH = 3200,
            VEHICLE_WIDTH = 1550,
            VEHICLE_HEIGHT = 200;

    /**
     * 板的三维
     */
    double
            BOARD_LENGTH = 5850,
            BOARD_WIDTH = 2200,
            BOARD_HEIGHT = 330;

    /**
     * 精调车的板三维
     */
    double
            V_BOARD_LENGTH = 2900,
            V_BOARD_WIDTH = 1350,
            V_BOARD_HEIGHT = 100,
            V_BOARD_INSTALL_X = 150,
            V_BOARD_INSTALL_Y = 100;

    /**
     * 千斤顶最大行程
     */
    double  ELECTRON_LENGTH = 200,
            HYDRAULIC_LENGTH = 150;


    /**
     * 支腿方向上千斤顶
     * 长边距离
     * 短边距离
     */
    double  V_JACK_LONG_DIS  = 2300,
            V_JACK_SHORT_DIS = 1300;

    AxisInfo R1_TOP = new AxisInfo(400, 1043.05, 0),
             R2_TOP = new AxisInfo(2800, 1043.05, 0),
             VEHICLE_CTR = new AxisInfo(1600, 775, 0);

    /**
     * 棱镜长宽
     * 4165.0 595 * 7
     * 5355 595 * 9
     */
    double LASER_LENGTH = 4165.,
            LASER_WIDTH = 1877.;

    double REFY_MAX_VAL = 4165 - 2300;
    double REFX_MAX_VAL = 1877 - 1300;

    /**
     * 板中心到测量孔的距离
     */
    double ROTATE_SPOT_DIS = M.dis(R1_TOP, VEHICLE_CTR);

    /**
     * 电缸到中心的距离
     */
    double R_JACK_DIS = 2400;

    double ZERO_POS_HEIGHT = 641.09;

    double LASER_LEN = 105.75;

    double CTR_JACK_BTM_DIS = 373;

    double R_JACK_LEN = 543;
}
