package com.jskj.fine_tune.tweak;

/**
 * 目标点位信息
 */
public class TargetPoint extends AxisInfo {
    int num;

    /**
     *
     */
    AxisInfo pointA;

    AxisInfo pointB;

    AxisInfo pointC;

    /**
     * 转化后的垂直千斤顶坐标
     * 在“抖一抖”之后可以确认
     */
    AxisInfo pointD;

    /**
     * 转化后的托盘坐标
     * 在“抖一抖”之后可以确认
     */
    AxisInfo pointE;

    /**
     * 相对与车板的坐标
     */
    double refX, refY;

    /**
     * 相对坐标系下测量点相对于板顶点的横纵偏差
     */
    AxisInfo boardDiff;

    public void applyRef() {
        this.refX = boardDiff.x;
        this.refY = boardDiff.y;
    }

    public TargetPoint(double x, double y, double z) {
        super(x, y, z);
    }

    public TargetPoint(AxisInfo ax) {
        super(ax.x, ax.y, ax.z);
    }

    public TargetPoint() {
    }

    /**
     * 接受来自全站仪的坐标
     * 这步进行单位转化 m => mm
     * @param x
     * @param y
     * @param z
     * @return
     */
    public static TargetPoint fromMachine(double x, double y, double z) {
        return new TargetPoint(x * 1000, y * 1000, z * 1000);
    }

    public static TargetPoint fromAxisInfo(AxisInfo ax) {
        return new TargetPoint(ax.x, ax.y, ax.z);
    }
}
