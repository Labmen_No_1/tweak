package com.jskj.fine_tune.tweak;

public class AxisInfo {
    protected double x, y, z;

    AxisInfo(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    AxisInfo() {}

    public static AxisInfo of(double x, double y, double z) {
        return M.multi(new AxisInfo(y, x, z), 1000);
    }

    public static AxisInfo of(AxisInfo ax) {
        return of(ax.x, ax.y, ax.z);
    }

    @Override
    public String toString() {
        return "new AxisInfo(" +
                       String.format("%.5f", x) +
                ", " + String.format("%.5f", y) +
                ", " + String.format("%.5f", z) +
                ')';
    }
}
