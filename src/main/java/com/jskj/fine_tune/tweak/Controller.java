package com.jskj.fine_tune.tweak;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.jskj.fine_tune.tweak.G.*;
import static com.jskj.fine_tune.tweak.M.*;
import static com.jskj.fine_tune.tweak.U.*;

@SuppressWarnings("all")
/**
 * 程序入口
 */
public class Controller {

    private static boolean TEST_ENV = true;
    private double tolerate;
    /**
     * 棱镜的坐标信息
     */
    private List<MeasurePoint> measurePointList = new ArrayList<>();

    /**
     * 目标的坐标信息
     */
    private List<TargetPoint> targetPointList = new ArrayList<>();

    /**
     * 车的信息
     */
    private Vehicle vehicle;

    /**
     * 板的基本信息
     */
    private Board board;

    /**
     * 抬升量
     */
    private double rasieUpVal;

    /**
     * 短边抬升量
     */
    private double shortRasieUpVal;
    /**
     * 长边抬升量
     */
    private double longRasieUpVal;


    /**
     * 抬升位置
     */
    private Side side;

    /**
     * 承轨台坡度信息
     */
    private Slope slope;

    /**
     * 板信息
     */
    private VehicleBoard vehicleBoard;

    /**
     * 千斤顶维护字典
     */
    Map<String, Jack> jackMap = new LinkedHashMap<>(8, 1);

    /**
     * 调板前的抬升 短/长 边再测量的坐标值
     */
    private MeasurePoint[] measureShortPoints, measureLongPoints;

    /**
     * 调板状态
     */
    private TweakState tweakState;

     /**
     * 抬升
     */
    public Controller raiseUp(Side side, double raiseUpVal) {
        this.side = side;
        if (side == Side.LONG) {
            longRasieUpVal = raiseUpVal;
        } else if (side == Side.SHORT) {
            shortRasieUpVal = raiseUpVal;
        } else {

        }
        return this;
    }

    public Controller raiseUp(Side side) { return raiseUp(side, this.rasieUpVal); }

    /**
     * 设置抬升后的坐标信息
     * @param arr
     */
    public void updateMeasurePoint(MeasurePoint... arr) {
        if (arr == null) {
            throw new IllegalStateException("check the number of your params size!!!");
        }
        MeasurePoint[] tempArr = new MeasurePoint[4];
        for (int i = 0, len = arr.length; i < len; ++i) {
            tempArr[i] = arr[i];
        }
        // 设置第四个点
        AxisInfo ax = get4thPoint(
                arr[0].toAxisInfo(),
                arr[1].toAxisInfo(),
                arr[2].toAxisInfo());
        tempArr[3] = MeasurePoint.fromAxisInfo(ax);
        if (this.side == Side.LONG) {
            this.measureLongPoints = tempArr;
        } else if (this.side == Side.SHORT) {
            this.measureShortPoints = tempArr;
        } else {
            throw new IllegalStateException("需要指定抬升方向");
        }
    }

    /**
     * 设置测量座标点
     * @param poins
     */
    public void setMeasurePoints(MeasurePoint... poins) {
        if (poins == null || poins.length < 3) throw new IllegalArgumentException("至少包含三个测量点位");
        // 首次调用
        if (this.measurePointList.isEmpty()) {
            for (int i = 0, len = poins.length; i < len; ++i) {
                MeasurePoint mp = poins[i];
                // 设置编号
                mp.num = (i + 1);
                // 设置车板的初始坐标
                mp.vBoardCoord = getVehicleBoardRefCroodByNum(mp.num);
                // 引用目标点
                mp.targetPoint = targetPointList.get(i);
                this.measurePointList.add(mp);
            }
            // 推算第四个点
            AxisInfo fourthPoint = get4thPoint(poins);
            MeasurePoint m4 = MeasurePoint.fromAxisInfo(fourthPoint);
            m4.num = 4;
            m4.vBoardCoord = getVehicleBoardRefCroodByNum(4);
            m4.targetPoint = targetPointList.get(3);
            this.measurePointList.add(m4);
        }
        // 重入调整
        else {
            // 修改 xyz
            for (int i = 0, len = poins.length; i < len; ++i) {
                MeasurePoint curMp = poins[i];
                MeasurePoint mp = this.measurePointList.get(i);
                mp.x = curMp.x; mp.y = curMp.y; mp.z = curMp.z;
            }
        }
    }

    /**
     * 开始拟和
     */
    public TweakResult fit() {
        TweakResult ret = new TweakResult();
        // 计算 B C 两点
        convMeasurePoint(this.measurePointList);
        this.measurePointList.stream().forEach(elm -> elm.printConvertRate());
        if (check()) {
            // 承轨台上表面与目标 xyz 的差值均在容忍范围内
            ret.setCode(100);
            return ret;
        }
        // 计算车板的坐标需要依赖 B C 两点的先行计算
        convRef2AbsCoord();
        // 首次调板要执行的方法
        if (this.tweakState.getNum() == 0) {
            refix();
            solve(true, 20);
            solve(false, 0);
            convTargetPoints2Board();
        }
        // 测量点（转化到车板上的）
        List<AxisInfo> measurePoints = measurePointList.stream().map(elm -> elm.pointE).collect(Collectors.toList());
        // 目标点（转化到车板上的）
        List<AxisInfo> targetPoints  = targetPointList.stream().map(elm -> elm.pointE).collect(Collectors.toList());
        // 投影点
        List<AxisInfo> reflectPoints = reflectTarget2Measure(measurePoints, targetPoints);
        dumpPoints2Code(
                "reflect",
                reflectPoints.get(0),
                reflectPoints.get(1),
                reflectPoints.get(2),
                reflectPoints.get(3)
        );
        dumpPoints2Code(
                "measurePointE",
                measurePoints.get(0),
                measurePoints.get(1),
                measurePoints.get(2),
                measurePoints.get(3)
        );

        dumpPoints2Code(
                "targetPointE",
                targetPoints.get(0),
                targetPoints.get(1),
                targetPoints.get(2),
                targetPoints.get(3)
        );

        // 计算平移量
        translate(measurePoints, reflectPoints, ret);
        if (ret.getCode() == 400) {
//            return ret;
        }
        // 计算旋转量
        rotate(measurePoints, reflectPoints, ret);
        if (ret.getCode() == 400) {
            return ret;
        }
        // 计算抬升量

        return ret;
    }

    private void translate(List<AxisInfo> measurePoints, List<AxisInfo> reflectPoints, TweakResult resp) {
        // 计算 relectPoints 的质心和  measurePoints 的质心
        AxisInfo rCenter = getCenter(reflectPoints);
        AxisInfo mCenter = getCenter(measurePoints);

        // 判断质心的x, y的差值
        AxisInfo delta = sub(mCenter, rCenter);
        print("平移分量 %s", delta);
        // 正数 需要向右移动
        // 负数 需要向左移动
        Jack h1 = jackMap.get("H1");
        double curDis = h1.curDistance + delta.x;
        // 判断千斤顶量程
        boolean goLeft = curDis > h1.maxDistance;
        boolean goRight = curDis < 0;

        if (goLeft || goRight) {
            resp.setCode(400);
            double val = 0;
            if (goLeft) {
                val = -Math.abs(curDis - h1.maxDistance);
            } else {
                val = Math.abs(curDis);
            }
            resp.setHorizonShift(val);
        } else {
            // 设置行程
            resp.getJackTweakMap().put("H1", delta.x);
        }

        // 判断 y 决定上下平移
        Jack    r1 = jackMap.get("R1"),
                r2 = jackMap.get("R2");
        double  r1CurDis = r1.curDistance - delta.y,
                r2CurDis = r2.curDistance - delta.y;

        boolean r1GoUp = r1CurDis > r1.maxDistance,
                r1GoDown = r1CurDis < 0;
        boolean r2GoUp = r2CurDis > r2.maxDistance,
                r2GoDown = r2CurDis < 0;

        if (r1GoDown || r1GoUp || r2GoDown || r2GoUp) {
            resp.setCode(400);
            double val = 0;
            if (r1GoUp && r2GoUp) {
                val = Math.max(Math.abs(r1CurDis - r1.maxDistance), Math.abs(r2CurDis - r2.maxDistance));
            } else if (r1GoDown && r2GoDown) {
                val = -Math.max(r1CurDis, r2CurDis);
            } else if (r1GoUp && r2GoDown) {

            } else if (r1GoDown && r2GoUp) {

            } else {

            }
            resp.setVerticalShift(val);
        } else {
            resp.getJackTweakMap().put("R1", delta.y);
            resp.getJackTweakMap().put("R2", delta.y);
        }
    }

    /**
     * TDO 旋转
     */
    private void rotate(List<AxisInfo> measurePoints, List<AxisInfo> reflectPoints, TweakResult resp) {
        // 选取 1， 3 两个点位取向量
        int num2 = 1, num3 = 2;
        AxisInfo mVec = sub(measurePoints.get(num3), measurePoints.get(num2));
        AxisInfo rVec = sub(reflectPoints.get(num3), reflectPoints.get(num2));

        Jack r1 = jackMap.get("R1"), r2 = jackMap.get("R2");
        // 求出向量夹角
        double angle = calcVecAngle(mVec, rVec);
        print(f("计算夹角为 %.10f deg", angle));
        // 计算旋转方向
        int dir = rotateDirection(mVec, rVec);
        print(f("旋转方向为 %s时针", dir == 1 ? "顺" : "逆"));
        if (dir == 0) return;
        String rNum = dir == 1 ? "R1" : "R2";
        // TODO 修改旋转方法
        Jack jack = jackMap.get(rNum);



    }

    private void roate0(List<AxisInfo> measurePoints, List<AxisInfo> reflectPoints, TweakResult resp) {
        // 选取 1， 3 两个点位取向量
        int num2 = 1, num3 = 2;
        AxisInfo mVec = sub(measurePoints.get(num3), measurePoints.get(num2));
        AxisInfo rVec = sub(reflectPoints.get(num3), reflectPoints.get(num2));

        Jack r1 = jackMap.get("R1"), r2 = jackMap.get("R2");
        // 求出向量夹角
        double angle = calcVecAngle(mVec, rVec);
        print(f("计算夹角为 %.2f deg", angle));
        // 计算旋转方向
        int dir = rotateDirection(rVec, mVec);
        print(f("旋转方向为 %s时针", dir == 1 ? "顺" : "逆"));
        if (dir == 0) return;
        String rNum = dir == 1 ? "R2" : "R1";

        Jack jack = jackMap.get(rNum);
        // 1. 计算旋转千斤顶的顶部坐标到千斤顶中心的距离
        double r1tNr2t = dis(R1_TOP, R2_TOP);
        // 等腰三角形的边长
        double traingleSideLen = dis(R1_TOP, VEHICLE_CTR);
        double rad = Math.toRadians(angle);
        double v = r1tNr2t * Math.sin(rad);
    }

    private void convTargetPoints2Board() {
        solve0(2);
    }

    /**
     * 用来展示板件的偏移情况
     *
     * 返回棱镜坐标点转化到板上的坐标
     * @return
     */
    public List<AxisInfo> show() {
        List<AxisInfo> ret = new ArrayList<>();
        for (MeasurePoint mp : measurePointList) {
            ret.add(mp.pointB);
        }
        return ret;
    }

    /**
     * 更新千斤顶行程
     */
    public void updateJackDistance(Map<String, Double> jackDistanceMap) {
        jackDistanceMap.forEach((k, v) -> {
            Jack jack = this.jackMap.get(k);
            jack.curDistance = v;
        });
    }

    /*-----------------------------------private method----------------------------------------------*/
    /**
     * 检查当前测量点与目标点的坐标差
     * @return
     */
    public boolean check() {
        boolean ret = true;
        for (MeasurePoint mp : measurePointList) {
            if (! mp.isDone(this.tolerate)) return false;
        }
        return ret;
    }

    /**
     * 转化测量点
     * @param btmFlag bool
     *                true  将测量点转化为承轨台底部的坐标
     *                false 将测量点转化为承轨台顶部的坐标
     */
    private void convMeasurePoint(List<MeasurePoint> measurePointList) {
        MeasurePoint
                p1 = measurePointList.get(0),
                p2 = measurePointList.get(1),
                p3 = measurePointList.get(2),
                p4 = measurePointList.get(3);

        // 计算测量 3 个点形成的平面
        Plane measurePlane = M.planeFit(p1, p2, p3);
        print(f("棱镜平面方程 %s", measurePlane.toString()));
        // 计算测量棱镜到板表面的高度
        double h = this.slope.calcHeight();
        print(f("测量棱镜到板表面的高度 %.2f", h));
        calcMeasurePoint(true, measurePlane, h, p1, p2, p3, p4);
        calcMeasurePoint(false, measurePlane, h, p1, p2, p3, p4);
        dumpPoints(p1, p2, p3, p4);
        System.out.println("转化后的 BC");
        dumpPoints(p1.pointB, p2.pointB, p3.pointB, p4.pointB);
        dumpPoints(p1.pointC, p2.pointC, p3.pointC, p4.pointC);
        System.out.println("========");
    }

    /**
     *
     * @param bOrc true B false C
     */
    private void calcMeasurePoint(boolean bOrc, Plane measurePlane, double height, AxisInfo... points) {
        double delta = bOrc ? height : (height + this.board.height);
        Plane plane = measurePlane.getUnderParallel(-delta);
        print(f("%s平面方程 %s", bOrc ? "表面": "底部", plane.toString()));
        AxisInfo
            a1 = getLineJointPlane(plane, points[0]),
            a2 = getLineJointPlane(plane, points[1]),
            a3 = getLineJointPlane(plane, points[2]),
            a4 = getLineJointPlane(plane, points[3]);
        dumpPoints2Code("a", a1, a2, a3);
        // 计算两点间的距离
        double disA1nA2 = dis(a1, a2), disA1nB1 = slope.calcDisAB();
        // 计算比例
        double rate = Math.abs(disA1nA2 / disA1nB1);
        int len = this.measurePointList.size();
        for (int i = 0; i < len; ++i) {
            MeasurePoint mp = measurePointList.get(i);
            AxisInfo x = new AxisInfo();
            switch (mp.num) {
                case 1:
                    x = sub(a1, divide(sub(a2, a1), rate));
                    if (bOrc) mp.pointA = a1;
                    break;
                case 2:
                    x = add(divide(sub(a2, a1), rate), a2);
                    if (bOrc) mp.pointA = a2;
                    break;
                case 3:
                    MeasurePoint first = measurePointList.get(0);
                    x = add(a3, sub(bOrc ? first.pointB : first.pointC, a1));
                    if (bOrc) mp.pointA = a3;
                    break;
                case 4:
                    if (bOrc) mp.pointA = a4;
                    break;
            }
            if (bOrc) {
                mp.pointB = x;
            } else {
                mp.pointC = x;
            }
        }
        // 设置第四个点
        MeasurePoint
                m1 = measurePointList.get(0),
                m2 = measurePointList.get(1),
                m3 = measurePointList.get(2);
        MeasurePoint fourthPoint = measurePointList.get(len - 1);
        fourthPoint.pointA = a4;
        if (bOrc) {
            fourthPoint.pointB = get4thPoint(m1.pointB, m2.pointB, m3.pointB);
            dumpPoints2Code("b", m1.pointB, m2.pointB, m3.pointB);
        } else {
            fourthPoint.pointC = get4thPoint(m1.pointC, m2.pointC, m3.pointC);
            dumpPoints2Code("c", m1.pointB, m2.pointB, m3.pointB);
        }
    }

    /**
     * 初始化检查程序
     * init 每个 controller 对象只用初始化执行一次
     */
    private void init() {
        // 1. 转换目标坐标
        // 目标坐标是指承轨台上表面的坐标，需要先转换到下表面的坐标 B 点
        if (TEST_ENV) {
            double h = this.slope.calcHeight();
            print(f("目标高度 %.2f", h));
            Plane surfacePlane = planeFit(targetPointList);
            Plane btmPlane = surfacePlane.getUnderParallel(-(board.height + h));

            for (TargetPoint tp : targetPointList) {
                AxisInfo a = getLineJointPlane(btmPlane, tp);
                tp.pointA = a;
            }

            AxisInfo a1 = targetPointList.get(0).pointA;
            AxisInfo a2 = targetPointList.get(1).pointA;
            AxisInfo a3 = targetPointList.get(2).pointA;

            double disA1nA2 = dis(a1, a2), disA1nB1 = slope.calcDisAB();
            // 计算比例
            double rate = Math.abs(disA1nA2 / disA1nB1);
            int len = this.targetPointList.size();
            for (int i = 0; i < len; ++i) {
                TargetPoint tp = targetPointList.get(i);
                AxisInfo x = new AxisInfo();
                switch (tp.num) {
                    case 1:
                        x = sub(a1, divide(sub(a2, a1), rate));
                        break;
                    case 2:
                        x = add(divide(sub(a2, a1), rate), a2);
                        break;
                    case 3:
                        TargetPoint first = targetPointList.get(0);
                        x = add(a3, sub(first.pointC, a1));
                        break;
                    case 4:
                        break;
                }
                System.out.println(">>>>>>>> break");
                tp.pointC = x;
            }

            // 设置第四个点
            TargetPoint t4 = targetPointList.get(3);
            System.out.println();
            t4.pointC = get4thPoint(targetPointList.get(0).pointC, targetPointList.get(1).pointC, targetPointList.get(2).pointC);
        } else {
            // TODO 生产环境
            // laserlen = 0 位于坡面中电
            convTargetPoint();
        }

    }

    /**
     * 为测量点设置对应的车板相对坐标
     * 方便做进一步对应转换计算
     * @param num 测量点编号
     * @return
     */
    private AxisInfo getVehicleBoardRefCroodByNum(int num) {
        double x = 0, y = 0, z = 0;
        switch (num) {
            case 1:
                x = (VEHICLE_LENGTH - V_BOARD_INSTALL_X);
                y = (VEHICLE_WIDTH - V_BOARD_INSTALL_Y);
                break;
            case 2:
                x = (VEHICLE_LENGTH - V_BOARD_INSTALL_X);
                y = V_BOARD_INSTALL_Y;
                break;
            case 3:
                x = V_BOARD_INSTALL_X;
                y = (VEHICLE_WIDTH - V_BOARD_INSTALL_Y);
                break;
            case 4:
                x = V_BOARD_INSTALL_X;
                y = V_BOARD_INSTALL_Y;
        }
        return new AxisInfo(x, y, z);
    }

    /**
     * 转换测量点与目标点的车板坐标
     */
    private void convRef2AbsCoord() {
        print("计算托盘的绝对坐标");
        // 假设短边抬升 34 长边抬升 13
        // 先计算短边
        int len = measurePointList.size();
        double[] deltaShortArr = new double[len],
                 deltaLongArr  = new double[len];

        // 计算抬升后、因高度变化求出的相对位置
        for (int i = 0; i < len; ++i) {
            MeasurePoint
                    mp = measurePointList.get(i),
                    msp = measureShortPoints[i],
                    mlp = measureLongPoints[i];

            TargetPoint tp = targetPointList.get(i);

            print(f("测量坐标 %s", mp.toString()));
            print(f("短边坐标 %s", msp.toString()));
            print(f("长边坐标 %s", mlp.toString()));

            // 用测量值减去初始值
            // 正数代表升高 负数代表下降
            double deltaShort = msp.z - mp.z;
            double deltaLong  = mlp.z - mp.z;
            deltaShortArr[i] = deltaShort;
            deltaLongArr[i] = deltaLong;

            // 计算相对距离
            double shortRate = (deltaShort / shortRasieUpVal), longRate = (deltaLong / longRasieUpVal);
            print(f("长短边抬升量 %.2f %.2f", shortRasieUpVal, longRasieUpVal));
            print(f("编号 %d 短边抬升量 %.2f, 长边抬升量 %.2f, 短边比率 %.2f, 长边比率 %.2f", (i+1), deltaShort, deltaLong, shortRate, longRate));
            /*mp.refX = (longRate - 1) * V_JACK_SHORT_DIS;
            mp.refY = (shortRate - 1) * V_JACK_LONG_DIS;*/
            // 判断抬升比率的正负情况
            print(f("长边比率 %.2f 计算过程 %.2f 距离 %.8f 计算值 %.2f", longRate, (longRate - 1), V_JACK_SHORT_DIS,
                    ((longRate - 1) * V_JACK_SHORT_DIS)));
            mp.jackDiff = new AxisInfo(
                    abs(longRate >= 0 ? (longRate - 1) * V_JACK_SHORT_DIS : longRate * V_JACK_SHORT_DIS),
                    abs(shortRate >= 0 ? (shortRate - 1) * V_JACK_LONG_DIS : shortRate * V_JACK_LONG_DIS),
                    0);

            double  refXdelta = (V_BOARD_WIDTH - V_JACK_SHORT_DIS) / 2,
                    refYdelta = (V_BOARD_LENGTH - V_JACK_LONG_DIS) / 2;
            AxisInfo boardDiff = new AxisInfo(
                    mp.jackDiff.x - refXdelta,
                    mp.jackDiff.y - refYdelta,
                    0
            );
            mp.boardDiff = boardDiff;

            tp.boardDiff = boardDiff;
            print(f("千斤顶 refX %.2f refY %.2f", mp.jackDiff.x, mp.jackDiff.y));
            print(f("托盘 refX %.2f refY %.2f", mp.boardDiff.x, mp.boardDiff.y));
        }
        // 计算 13 两点的抬升量
        double shortVal = abs(deltaShortArr[len - 1] - deltaShortArr[0]);
        // 计算 12 两点的抬升量
        double longVal = abs(deltaLongArr[0] - deltaLongArr[1]);
        print(f("短边总抬升量 %.2f 长边总抬升量 %.2f", shortVal, longVal));
        print(f("短边各个点的变化量 %s", Arrays.toString(deltaShortArr)));
        print(f("长边各个点的变化量 %s", Arrays.toString(deltaLongArr)));
    }

    /**
     * 修正因为抖动问题产生的误差
     */
    private void refix() {
        // 计算
        MeasurePoint m1 = measurePointList.get(0);
        MeasurePoint m2 = measurePointList.get(1);
        MeasurePoint m3 = measurePointList.get(2);
        MeasurePoint m4 = measurePointList.get(3);
        // c2 2
        /*double m = abs((measureShortPoints[0].z - m1.z) - (measureShortPoints[2].z - m3.z));
        double disC2X2 = V_JACK_LONG_DIS * (m / shortRasieUpVal);
        // c1 3 短
        // 1 3 两点的抬升量
        double disC1X3 = disC2X2 * LASER_WIDTH / LASER_LENGTH;*/

        // 测试短边
        double m = abs((measureLongPoints[0].z - m1.z) - (measureLongPoints[1].z - m2.z));
        double disC1X3 = V_JACK_SHORT_DIS * (m / longRasieUpVal);
        // c1 3 短
        // 1 3 两点的抬升量
        double disC2X2 = disC1X3 * LASER_LENGTH / LASER_WIDTH ;

        AxisInfo m1JackDiff = m1.jackDiff;
        AxisInfo m2JackDiff = m2.jackDiff;
        AxisInfo m3JackDiff = m3.jackDiff;
        AxisInfo m4JackDiff = m4.jackDiff;
        // fix val
        double fixValX = (disC2X2 - V_JACK_SHORT_DIS - m1JackDiff.x - m3JackDiff.x) / 2;
        double fixValY = (disC1X3 - V_JACK_LONG_DIS - m1JackDiff.y - m2JackDiff.y) / 2;

        // 补偿误差值
        for (MeasurePoint mp : measurePointList) {
            AxisInfo jd = mp.jackDiff;
            AxisInfo bd = mp.boardDiff;
            print(f("jack diff before %s", jd.toString()));
            print(f("board diff before %s", bd.toString()));
            jd.x += fixValX;
            jd.y += fixValY;
            bd.x += fixValX;
            bd.y += fixValY;
            print(f("jack diff after %s", jd.toString()));
            print(f("board diff after %s", bd.toString()));
        }
    }

    /**
     * 等比法推算4个千斤顶中心的绝对坐标
     *
     * @param isD true 代表垂直抬升的千斤顶
     * @param vBoardHeight 托盘的厚度
     */
    private void solve(boolean isD, double vBoardHeight) {
        MeasurePoint
                m1 = measurePointList.get(0),
                m2 = measurePointList.get(1),
                m3 = measurePointList.get(2),
                m4 = measurePointList.get(3);
        AxisInfo
                c1 = m1.pointC,
                c2 = m2.pointC,
                c3 = m3.pointC,
                c4 = m4.pointC;
        print("转化后的坐标");
        dumpPoints(c1, c2, c3, c4);

        double longSide = isD ? V_JACK_LONG_DIS : V_BOARD_LENGTH;
        double shortSide = isD ? V_JACK_SHORT_DIS : V_BOARD_WIDTH;
        Stream.of(m1, m2, m3, m4).forEach(elm -> {
            elm.changeRef(isD);
            print(f("%srefx %.8f refy %.8f", isD ? "": "<<<<<<", elm.refX, elm.refY));
        });

        // 求 1 点
        // 先求夹角用 m4-m3 的 refY 的分量
        double delta = m4.refY - m3.refY;
        print(f("m4-m3 refY %.2f", delta));
        print(f("m4 refx %.2f", m4.refX));
        double disC1nC3 = LASER_LENGTH;
        double disC3nC4 = LASER_WIDTH;
        print(f("disC1nC3 %.2f", disC3nC4));
        double rad = Math.asin(abs(delta / disC3nC4));
        print(f("夹角是 %.2f", Math.toDegrees(rad)));

        double c = delta / Math.cos(rad);
        print(f("三角形斜边 %.2f", c));
        double rate = disC1nC3 / c;
        print(f("比率 %.2f", rate));
        // sub(c1, c3) / sub(c3, x) = rate
        AxisInfo p1 = sub(c3, divide(sub(c1, c3), rate));
        print(f("p1 %s", p1.toString()));

        // 2
        // TODO 三角函数再验证减少累计误差
        double disC4nP1 = LASER_WIDTH / Math.cos(rad);
        print(f("disC4nP1 %.2f", disC4nP1));
        // 求短边
        double a = c * Math.sin(rad);
        print(f("短边 %.2f", a));
        rate = disC4nP1 / a;
        // sub(c4, p1) / sub(x, p1) = disC4nP1 / a
        AxisInfo p2 = add(p1, divide(sub(c4, p1), rate));
        print(f("p2 %s", p2.toString()));

        // 3
        // TODO 直接使用 m3 点的 refX 偏差
        double disP3nP2 = m3.refX;
        print(f("disP3nP2 %.2f", disP3nP2));
        rate = disP3nP2 / a;
        print(f("rate %.2f", rate));
        // sub(p3, p2) / sub(p2, p1) = rate
        AxisInfo p3 = add(p2, multi(sub(p2, p1), rate));
        print(f("p3 %s", p3.toString()));

        // 4
        AxisInfo p4 = get4thPoint(p2, c3, p3);
        print(f("p4 %s", p4.toString()));

        // 5
        double disP3nP4 = m4.refY - m3.refY;
        print(f("disP3nP4 %.2f", disP3nP4));
        // m3 点的 refY
        double disP4nP5 = m3.refY;
        print(f("disP4nP5 %.2f", disP4nP5));
        rate = disP4nP5 / disP3nP4;
        print(f("rate %.2f", rate));
        // sub(p5, p4) / sub(p4, p3) = disP4nP5 / disP3nP4
        AxisInfo p5 = add(p4, multi(sub(p4, p3), rate));
        print(f("p5 %s", p5.toString()));

        // 6
        double disC4nP6 = m4.refX;
        print(f("disC4nP6 %.2f", disC4nP6));
        // TODO m4.refX + 支腿宽度减少误差
        double disC4nP3 = m4.refX + shortSide;
        print(f("disC4nP3 %.2f", disC4nP3));
        rate = disC4nP6 / disC4nP3;
        print(f("rate %.2f", rate));
        // sub(p6, c4) / sub(p3, c4) = rate
        AxisInfo p6 = add(c4, multi(sub(p3, c4), rate));
        print(f("p6 %s", p6.toString()));

        // 7
        AxisInfo p7 = get4thPoint(p3, p5, p6);
        print(f("p7 %s", p7.toString()));

        // 8
        rate = disP4nP5 / longSide;
        print(f("rate %.2f", rate));
        // sub(p5, p4) / sub(p8, p5) = rate
        AxisInfo p8 = add(p5, divide(sub(p5, p4), rate));
        print(f("p8 %s", p8.toString()));

        // p9
        AxisInfo p9 = sub(p8, sub(p5, p7));
        print(f("p9 %s", p9.toString()));

        // pointD or pointE
        if (isD) {
            m1.pointD = p8;
            m2.pointD = p9;
            m3.pointD = p5;
            m4.pointD = p7;
        } else {
            m1.pointE = p8;
            m2.pointE = p9;
            m3.pointE = p5;
            m4.pointE = p7;
        }
        print(f("%s 的坐标分别是：", isD ? "千斤顶中心" : "托盘顶点"));
        dumpPoints2Code(isD ? "d" : "e", p8, p9, p5, p7);
        print(f("36两点间的距离 %.2f", dis(p3, p6)));
        if (isD) {
            print(f("千斤顶距离 12 %.2f 13 %.2f", dis(p8, p9), dis(p8, p5)));
        }
    }

    private void solve0(double vBoardHeight) {
        TargetPoint
                m1 = targetPointList.get(0),
                m2 = targetPointList.get(1),
                m3 = targetPointList.get(2),
                m4 = targetPointList.get(3);
        AxisInfo
                c1 = m1.pointC,
                c2 = m2.pointC,
                c3 = m3.pointC,
                c4 = m4.pointC;
        print("转化后的坐标");
        dumpPoints(c1, c2, c3, c4);

        double longSide =  V_BOARD_LENGTH;
        double shortSide = V_BOARD_WIDTH;
        Stream.of(m1, m2, m3, m4).forEach(elm -> {
            elm.applyRef();
            print(f(">>>>> refx %.2f refy %.2f", elm.refX, elm.refY));
        });

        // 求 1 点
        // 先求夹角用 m4-m3 的 refY 的分量
        double delta = m4.refY - m3.refY;
        print(f("m4-m3 refY %.2f", delta));
        print(f("m4 refx %.2f", m4.refX));
        double disC1nC3 = LASER_LENGTH;
        double disC3nC4 = LASER_WIDTH;
        print(f("disC1nC3 %.2f", disC3nC4));
        double rad = Math.asin(abs(delta / disC3nC4));
        print(f("夹角是 %.2f", Math.toDegrees(rad)));

        double c = delta / Math.cos(rad);
        print(f("三角形斜边 %.2f", c));
        double rate = disC1nC3 / c;
        print(f("比率 %.2f", rate));
        // sub(c1, c3) / sub(c3, x) = rate
        AxisInfo p1 = sub(c3, divide(sub(c1, c3), rate));
        print(f("p1 %s", p1.toString()));

        // 2
        // TODO 三角函数再验证减少累计误差
        double disC4nP1 = LASER_WIDTH / Math.cos(rad);
        print(f("disC4nP1 %.2f", disC4nP1));
        // 求短边
        double a = c * Math.sin(rad);
        print(f("短边 %.2f", a));
        rate = disC4nP1 / a;
        // sub(c4, p1) / sub(x, p1) = disC4nP1 / a
        AxisInfo p2 = add(p1, divide(sub(c4, p1), rate));
        print(f("p2 %s", p2.toString()));

        // 3
        // TODO 直接使用 m3 点的 refX 偏差
        double disP3nP2 = m3.refX;
        print(f("disP3nP2 %.2f", disP3nP2));
        rate = disP3nP2 / a;
        print(f("rate %.2f", rate));
        // sub(p3, p2) / sub(p2, p1) = rate
        AxisInfo p3 = add(p2, multi(sub(p2, p1), rate));
        print(f("p3 %s", p3.toString()));

        // 4
        AxisInfo p4 = get4thPoint(p2, c3, p3);
        print(f("p4 %s", p4.toString()));

        // 5
        double disP3nP4 = m4.refY - m3.refY;
        print(f("disP3nP4 %.2f", disP3nP4));
        // m3 点的 refY
        double disP4nP5 = m3.refY;
        print(f("disP4nP5 %.2f", disP4nP5));
        rate = disP4nP5 / disP3nP4;
        print(f("rate %.2f", rate));
        // sub(p5, p4) / sub(p4, p3) = disP4nP5 / disP3nP4
        AxisInfo p5 = add(p4, multi(sub(p4, p3), rate));
        print(f("p5 %s", p5.toString()));

        // 6
        double disC4nP6 = m4.refX;
        print(f("disC4nP6 %.2f", disC4nP6));
        // TODO m4.refX + 支腿宽度减少误差
        double disC4nP3 = m4.refX + shortSide;
        print(f("disC4nP3 %.2f", disC4nP3));
        rate = disC4nP6 / disC4nP3;
        print(f("rate %.2f", rate));
        // sub(p6, c4) / sub(p3, c4) = rate
        AxisInfo p6 = add(c4, multi(sub(p3, c4), rate));
        print(f("p6 %s", p6.toString()));

        // 7
        AxisInfo p7 = get4thPoint(p3, p5, p6);
        print(f("p7 %s", p7.toString()));

        // 8
        rate = disP4nP5 / longSide;
        print(f("rate %.2f", rate));
        // sub(p5, p4) / sub(p8, p5) = rate
        AxisInfo p8 = add(p5, divide(sub(p5, p4), rate));
        print(f("p8 %s", p8.toString()));

        // p9
        AxisInfo p9 = sub(p8, sub(p5, p7));
        print(f("p9 %s", p9.toString()));

        // pointD or pointE
        m1.pointE = p8;
        m2.pointE = p9;
        m3.pointE = p5;
        m4.pointE = p7;
        print(f("%s 的坐标分别是：", "托盘顶点"));
        dumpPoints2Code("tgtE", p8, p9, p5, p7);
        print(f("======36两点间的距离 %.2f", dis(p3, p6)));

        print(f("千斤顶距离 12 %.2f 13 %.2f", dis(p8, p9), dis(p8, p5)));
    }

    /**
     * 通过目标点位作垂线交于测量平面
     * @return
     */
    private List<AxisInfo> reflectTarget2Measure(List<AxisInfo> measurePoints,
                                                 List<AxisInfo> targetPoints) {
        List<AxisInfo> ret = new ArrayList<>();
        Plane mPlane = planeFit(measurePoints);
        print(f("测量平面 %s", mPlane.toString()));
        Plane tPlane = planeFit(targetPoints);
        print(f("目标平面 %s", tPlane.toString()));
        // 求二面角
        double angle = twoPlaneAngle(mPlane, tPlane);
        print(f("二面角 %.10f", angle));
        // 求两个平面的相交直线
        Line line = twoPlaneJoint(mPlane, tPlane);
        print(f("交线 %s", line));
        // 旋转目标的托盘坐标
        for (TargetPoint tp : targetPointList) {
            AxisInfo e = tp.pointE;
            AxisInfo reflectPoint;
            print(f("目标点 %s", e.toString()));
            if (line != null) {
                AxisInfo footPoint = disBetweenPointAndLineFootPoint(e, line);
                double point2lineDis = dis(footPoint, e);
                reflectPoint = rotatePointByLine(e, footPoint, mPlane);
                // 目标点距离线的距离
                double rdSideLength = get3rdSideLength(point2lineDis, point2lineDis, angle);
                print(f("等腰三角形 底边距离 %.2f", rdSideLength));
                print(f("等腰三角形 底边距离0 %.2f", dis(reflectPoint, e)));
                print(f("目标距离交线的距离 %.2f", point2lineDis));
            } else {
                // 测量与目标平行
                reflectPoint = getLineJointPlane(mPlane, e);
            }
            ret.add(reflectPoint);
        }
        return ret;
    }

    /**
     * 目标点在坡面中点,
     * 转化到棱镜对应的最高孔位B点与板上表面的交点
     */
    public void convTargetPoint() {
        // 1. 计算棱镜形成的平面
        TargetPoint t1 = targetPointList.get(0);
        TargetPoint t2 = targetPointList.get(1);
        TargetPoint t3 = targetPointList.get(2);
        TargetPoint t4 = targetPointList.get(3);
        Plane laserPlane = planeFit(t1, t2, t3);
        // 计算中位棱镜距离点B的距离
        // 计算中位棱镜距里板表面的距离
        double h = 12.3 / 2 + 23.7;
        // 垂直投影
        Plane btmPlane = laserPlane.getUnderParallel(-(h + this.board.height));
        // 交点
        AxisInfo l1 = getLineJointPlane(btmPlane, t1);
        AxisInfo l2 = getLineJointPlane(btmPlane, t2);
        AxisInfo l3 = getLineJointPlane(btmPlane, t3);
        AxisInfo l4 = getLineJointPlane(btmPlane, t4);
        // 计算 B 点
        double disL = dis(l1, l2), disLB = this.slope.len / 2 - 57;
        double total = disL + disLB;
        // b1 - l2 / l1-l2 = total / disL
        AxisInfo c1 = add(multi(sub(l1, l2), (total / disL)), l2);
        // l1 - b2 / l1 - l2 = disL / total
        AxisInfo c2 = sub(l1, multi(sub(l1, l2), (disL / total)));
        AxisInfo c3 = add(l3, sub(c1, l1));
        AxisInfo c4 = get4thPoint(c1, c2, c3);
        // 赋值
        t1.pointC = c1;
        t2.pointC = c2;
        t3.pointC = c3;
        t4.pointC = c4;
    }

    /*-------------------------------------builder--------------------------------------------*/
    private Controller(Builder builder) {
        this.rasieUpVal = builder.raiseUpVal;

        this.vehicle = builder.vehicle;
        this.board = builder.board;
        this.slope = builder.slope;
        this.vehicleBoard = builder.vehicleBoard;
        this.tolerate = builder.tolerate;

        // 初始化调板状态
        this.tweakState = new TweakState();

        int len = builder.targetPointArr.length;
        for (int i = 0; i < len; ++i) {
            TargetPoint tgpt = builder.targetPointArr[i];
            tgpt.num = (i + 1);
            this.targetPointList.add(tgpt);
        }
        // 第四个目标点
        AxisInfo fourthPoint = get4thPoint(builder.targetPointArr);
        TargetPoint t4 = TargetPoint.fromAxisInfo(fourthPoint);
        t4.num = 4;
        this.targetPointList.add(t4);

        len = builder.vJackArr.length;
        List<Jack> vJacks = new ArrayList<>();
        for (int i = 0; i < len; ++i) {
            Jack jack = builder.vJackArr[i];
            jack.num = (i + 1);
            vJacks.add(jack);
            jackMap.put(jack.getJackName(), jack);
        }
        this.vehicle.vJacks = vJacks;

        len = builder.rJackArr.length;
        List<Jack> rJacks = new ArrayList<>();
        for (int i = 0; i < len; ++i) {
            Jack jack = builder.rJackArr[i];
            jack.num = (i + 1);
            rJacks.add(jack);
            jackMap.put(jack.getJackName(), jack);
        }
        this.vehicle.rJacks = rJacks;

        this.vehicle.hJack = builder.hJack;
        builder.hJack.num += 1;
        jackMap.put(builder.hJack.getJackName(), builder.hJack);
        init();
    }

    public static class Builder {
        private double raiseUpVal;
        private MeasurePoint[] measurePointArr;
        private TargetPoint[] targetPointArr;
        private Vehicle vehicle = Vehicle.getDefault();
        private Board board = Board.getDefault();
        private Jack[] vJackArr;
        private Jack[] rJackArr;
        private Jack hJack;
        private double tolerate;
        private Slope slope = Slope.getDefault();
        private VehicleBoard vehicleBoard = VehicleBoard.getDefault();

        public Builder setRaiseUpVal(double val) {
            this.raiseUpVal = val;
            return this;
        }

        public Builder setMeasurePointArr(MeasurePoint... arr) {
            this.measurePointArr = arr;
            return this;
        }

        public Builder setTargetPointArr(TargetPoint... arr) {
            this.targetPointArr = arr;
            return this;
        }

        public Builder setVehicle(Vehicle obj) {
            this.vehicle = obj;
            return this;
        }

        public Builder setVJackArr(Jack... arr) {
            this.vJackArr = arr;
            return this;
        }

        public Builder setRJackArr(Jack... arr) {
            this.rJackArr = arr;
            return this;
        }

        public Builder setHJackArr(Jack obj) {
            this.hJack = obj;
            return this;
        }

        public Builder setTolerate(double tolerate) {
            this.tolerate = tolerate;
            return this;
        }

        public Builder setBoard(Board board) {
            this.board = board;
            return this;
        }

        public Builder setSlope(Slope slope) {
            this.slope = slope;
            return this;
        }

        public Builder setVehicleBoard(VehicleBoard vehicleBoard) {
            this.vehicleBoard = vehicleBoard;
            return this;
        }

        public Controller build() {
            return new Controller(this);
        }

    }

}
