package com.jskj.fine_tune.tweak;

import java.util.List;

import static java.lang.StrictMath.*;

/**
 * for math
 * @author Bowen Ren
 */
public final class M {

    /**
     * 求平均
     * @return float64
     */
    public static double avg(double... vals) {
        int len = vals.length;
        return sum(vals) / len;
    }

    public static double abs(double val) {
        return Math.abs(val);
    }

    /**
     * 求和
     * @return float64
     */
    public static double sum(double... vals) {
        int ret = 0; for (double val : vals) ret += val; return ret;
    }

    /**
     * 点积
     * @return float64
     */
    public static double dot(AxisInfo a, AxisInfo b) {
        return (a.x * b.x + a.y * b.y + a.z * b.z);
    }

    /**
     * 叉积
     * @return vec
     */
    public static AxisInfo cross(AxisInfo a, AxisInfo b) {
        double  x = (a.y * b.z - a.z * b.y),
                y = (a.z * b.x - a.x * b.z),
                z = (a.x * b.y - a.y * b.x);
        return Point(x, y, z);
    }

    /**
     * 向量的范数
     * @return float64
     */
    public static double norm(AxisInfo a) {
        return norm(a.x, a.y, a.z);
    }

    public static double norm(double... params) {
        double  x = params[0],
                y = params[1],
                z = params[2];
        return sqrt(x * x + y * y + z * z);
    }

    /**
     * 向量减法
     * @return vec
     */
    public static AxisInfo sub(AxisInfo a, AxisInfo b) {
        double  x = (a.x - b.x),
                y = (a.y - b.y),
                z = (a.z - b.z);
        return Point(x, y, z);
    }

    /**
     * 向量加法
     * @return vec
     */
    public static AxisInfo add(AxisInfo a, AxisInfo b) {
        double  x = (a.x + b.x),
                y = (a.y + b.y),
                z = (a.z + b.z);
        return Point(x, y, z);
    }

    public static AxisInfo divide(AxisInfo vec, double num) {
        double  x = vec.x / num,
                y = vec.y / num,
                z = vec.z / num;
        return Point(x, y, z);
    }

    public static AxisInfo multi(AxisInfo vec, double num) {
        double  x = vec.x * num,
                y = vec.y * num,
                z = vec.z * num;
        return Point(x, y, z);
    }

    /**
     * 中点
     * @param a
     * @param b
     * @return
     */
    public static AxisInfo mid(AxisInfo a, AxisInfo b) {
        return divide(add(a, b), 2);
    }

    /**
     * 平面拟合
     *
     * @param points three points
     * @return a Plane
     */
    public static Plane planeFit(AxisInfo... points) {
        if (points == null || points.length < 3) throw new IllegalArgumentException("at least three points can fit a plane!");

        AxisInfo p1 = points[0],
                 p2 = points[1],
                 p3 = points[2];

        AxisInfo vec1 = sub(p2, p1),
                 vec2 = sub(p3, p1);
        // normal vec
        AxisInfo n = cross(vec1, vec2);
        double   d = -dot(n, p1);
        Plane plane = Plane(n, d);
        // 为每个平面设置一个面点
        plane.point = p1;
        return plane;
    }

    public static Plane planeFit(List<?> points) {
        AxisInfo p1 = (AxisInfo) points.get(0),
                p2 = (AxisInfo) points.get(1),
                p3 = (AxisInfo) points.get(2);
        return planeFit(p1, p2, p3);
    }

    /**
     * 两点间的距离
     * @return float64
     */
    public static double dis(AxisInfo a, AxisInfo b) {
        return norm(sub(a, b));
    }

    public static double toDeg(double radVal) {
        double degrees = Math.toDegrees(radVal);
        return degrees > 90 ? (180 - 90) : degrees;
    }

    /**
     * 点到平面的距离
     *
     * @param plane 平面方程
     * @param point 点
     *
     * @return float64
     */
    public static double dis(Plane plane, AxisInfo point) {
        AxisInfo normalVec = plane.getNormalVec();
        double numerator  = abs(dot(normalVec, point) + plane.d), denominator = norm(normalVec);
        return denominator == 0. ? 0 : (numerator / denominator);
    }


    /**
     * 求垂直于平面的直线方程
     *
     * @param plane 平面对象
     * @param point 点
     * @return point obj
     */
    public static Plane getPerpendicularLine(Plane plane, AxisInfo point) {
        AxisInfo vec = plane.getNormalVec();
        double magnitude = norm(vec);
        // 计算出直线方向
        AxisInfo normalVec = divide(vec, magnitude);
        // 求出 d 值
        double d = -dot(normalVec, point);
        // 直线方程 也由 4 个系数组成，这里直接复用平面方程的对象
        return Line(normalVec, d);
    }

    /**
     * 获取直线与平面的交点
     *
     * @param line 直线方程
     * @param plane 平面方程
     *
     * @return Point obj may be null
     */
    public static AxisInfo getIntersectionPoint(Plane plane, Plane line) {
        AxisInfo planeVec = plane.getNormalVec(),
                 lineVec  = line.getNormalVec();
        // 计算点积
        double dp = dot(planeVec, lineVec);
        // 如果点积为 0 代表直线与平面不相交
        if (dp == 0.) {
            U.print("no intersection");
            return null;
        }
        // 求出系数 t
        double t = -sum(line.d, plane.a, plane.b, plane.c) / dp;
        return multi(lineVec, t);
    }

    /**
     * 过平面一点的垂线与另外的平面的交点
     * @param line
     * @param plane
     * @param point
     * @return
     */
    public static AxisInfo lineJointPlaneByPassPoint(Plane line, Plane plane, AxisInfo point) {
        AxisInfo lineDirect = line.getNormalVec();
        AxisInfo normalVec = plane.getNormalVec();
        double t = -sum(dot(normalVec, point), plane.d) / dot(normalVec, lineDirect);
        return add(point, multi(lineDirect, t));
    }

    /**
     * 过给定点并且垂直于已知平面的交点
     *
     * @param plane 平面方程 a b c d
     * @param point 点 xyz
     * @return 交点 xyz
     */
    public static AxisInfo getLineJointPlane(Plane plane, AxisInfo point) {
        AxisInfo n = plane.getNormalVec();
        double t = -(dot(n, point) + plane.d) / dot(n, n);
        return add(point, multi(n, t));
    }

    /**
     * 获取第四个点的座标
     * @param points
     * @return
     */
    public static AxisInfo get4thPoint(AxisInfo... points) {
        AxisInfo p1 = points[0],
                 p2 = points[1],
                 p3 = points[2];
        return add(sub(p2, p1), p3);
    }

    /**
     * TODO 改用象限
     * a 到 b 旋转的方向
     * 0 不需要旋转
     * 1 顺时针
     * -1 逆时针
     * @param a
     * @param b
     * @return int 0 1 -1
     */
    public static int rotateDirection(AxisInfo a, AxisInfo b) {
        // 叉乘的结果是正数，说明a到b是逆时针，反之顺时针；
        double crossVal = a.x * b.y - a.y * b.x;
        if (crossVal == 0) return 0;
        else if (crossVal > 0) return -1;
        else return 1;
    }

    /**
     * 计算二面角
     * @return 角度 double
     */
    public static double twoPlaneAngle(Plane a, Plane b) {
        // 分别求出两个平面的法向量
        AxisInfo aNorm = a.getNormalVec();
        AxisInfo bNorm = b.getNormalVec();

        // 计算法向量点积
        double dp = dot(aNorm, bNorm);
        // 计算向量的模
        double aMagnitude = norm(aNorm);
        double bMagnitude = norm(bNorm);
        double divide = aMagnitude * bMagnitude;
        if (divide == 0) return 0.;
        // 计算夹角余弦
        double deg = Math.toDegrees(Math.acos(dp / divide));
        return deg > 90 ? (180 - deg) : deg;
    }

    /**
     * 两平面相交的直线
     * @return
     */
    public static Line twoPlaneJoint(Plane p1, Plane p2) {
        double  a = p1.a,
                b = p1.b,
                c = p1.c,
                d = p1.d;
        double  e = p2.a,
                f = p2.b,
                g = p2.c,
                h = p2.d;
        // (x + a) / d
        // t = x + {(df - bh) / (af - be)} / {(bg - cf) / (af - be)}
        double val = (a * f - b * e);
        if (val == 0.) return null;
        double a1 = (d * f - b * h) / val;
        double d1 = (b * g - c * f) / val;
        // (y + b) / e
        // t = y + ((- de + ah) / (af - be)) / ((ce - ag) /  (af - be))
        double b1 = (-d * e + a * h) / val;
        double e1 = (c * e - a * g) / val;
        double c1 = 0;
        double f1 = 1;

        AxisInfo linePoint = new AxisInfo(a1, b1, c1);
        AxisInfo lineDirection = new AxisInfo(d1, e1, f1);
        return new Line(linePoint, lineDirection);
    }

    /**
     * 点到直线的距离
     * @param point 点
     * @param line 直线
     * @return
     */
    public static AxisInfo disBetweenPointAndLineFootPoint(AxisInfo point, Line line) {
        double
                a = line.point.x , b = line.point.y,  c = line.point.z,
                d = line.directionVec.x, e = line.directionVec.y, f = line.directionVec.z;
        double x = point.x, y = point.y, z = point.z;

        double t = (d * a + d * x + e * b + e * y + z) / (d * d + e * e + 1);
        AxisInfo p = new AxisInfo(
                d * t - a,
                e * t - b,
                t
        );
        return p;
    }


    /**
     * 点绕直线旋转
     * @return
     */
    public static AxisInfo rotatePointByLine(AxisInfo p1, AxisInfo footPoint, Plane plane) {
        // 过 p1 垂直于下平面
        AxisInfo p2 = getLineJointPlane(plane, p1);
        double a = dis(p1, footPoint),
                b = dis(p2, footPoint);
        double rate = b / a;
        return add(divide(sub(p2, footPoint), rate), footPoint);
    }

    /**
     * 计算质心
     * @param points
     * @return
     */
    public static AxisInfo getCenter(List<AxisInfo> points) {
        final int len = points.size();
        return points.stream()
                .reduce(M::add)
                .map(elm -> divide(elm, len))
                .orElse(new AxisInfo());
    }

    public double deg(double rad) {
        return StrictMath.toDegrees(rad);
    }

    public double rad(double deg) {
        return StrictMath.toRadians(deg);
    }

    /**
     * 计算两个向量的夹角
     * @return
     */
    public static double calcVecAngle(AxisInfo vec1, AxisInfo vec2) {
        double deg = calcVecAngle0(vec1, vec2);
        return deg > 90 ? (180 - deg) : deg;
    }

    public static double calcVecAngle0(AxisInfo vec1, AxisInfo vec2) {
        double dp = dot(vec1, vec2);
        double divide = norm(vec1) * norm(vec2);
        if (divide == 0) return 0.;
        double cosAngle = dp / divide;
        return Math.toDegrees(Math.acos(cosAngle));
    }

    public static double checkPointIsOnPlane(Plane plane, AxisInfo point) {
        return dot(plane.getNormalVec(), point) + plane.d;
    }

    /**
     * 点到平面的距离
     * @return
     */
    public static double disBetweenPointAndPlane(AxisInfo point, Plane plane) {
        AxisInfo vec = plane.getNormalVec();
        return abs(dot(point, vec) + plane.d) / norm(vec);
    }

    /*---------------------------------------------------三角形计算工具-------------------------------------------------*/

    /**
     * 已知三角形的两条边长以及两条边长的夹角，求出第三条边长
     *
     * @param angle 夹角的度数
     * @return float64
     */
    public static double get3rdSideLength(double a, double b, double angle) {
        // 参考公式
        // c^2 = a^2 + b^2 - 2ab * cos(C)
        return sqrt(a * a + b * b - 2 * a * b * cos(toRadians(angle)));
    }

    public static Line getLineByTwoPoint(AxisInfo p1, AxisInfo p2) {
        AxisInfo dir = sub(p2, p1);
        return new Line(p1, dir);
    }

    /**
     * 已知三角形的三条边长以及两条边长的夹角，求出第三条边长对应的角度
     *
     * @return float64 angle degrees
     */
    public static double get3rdSideAngle(double a, double b, double c) {
        // 参考公式
        // cos(C) = (a^2 + b^2 - c^2) / (2 * a * b)
        double cosVal = (a * a + b * b - c * c) / (2 * a * b);
        return toDegrees(acos(cosVal));
    }

    public static AxisInfo Point(double x, double y, double z) {
        return new AxisInfo(x, y, z);
    }

    private static Plane Plane(double a, double b, double c, double d) {
        return new Plane(a, b, c, d);
    }

    private static Plane Plane(AxisInfo vec, double d) {
        return new Plane(vec.x, vec.y, vec.z, d);
    }
    private static Plane Line(AxisInfo vec, double d) {
        return Plane(vec, d);
    }
}
