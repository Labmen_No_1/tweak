package com.jskj.fine_tune.tweak;

import java.util.ArrayList;
import java.util.List;

import static com.jskj.fine_tune.tweak.G.DUB_MOD;
import static com.jskj.fine_tune.tweak.M.*;
/**
 * 目标点组
 */
public class TargetGroup extends PointGroup {
    public TargetGroup(Slope slope, AxisInfo... points) {
        super(slope, points);
    }

    @Override
    protected void calcPointC() {

        if (DUB_MOD) {
            double h = slope.calcHeight() + G.BOARD_HEIGHT;
            cList = commonStep(h);
        } else {
            // 正式环境使用
            cList = prodEnv();
        }
    }

    private List<AxisInfo> prodEnv() {
        List<AxisInfo> ret = new ArrayList<>();
        double h = 12.3 / 2 + 23.7 + G.BOARD_HEIGHT;
        Plane btmPlane = pointPlane.getUnderParallel(h);
        AxisInfo l1 = getLineJointPlane(btmPlane, points.get(0));
        AxisInfo l2 = getLineJointPlane(btmPlane, points.get(1));
        AxisInfo l3 = getLineJointPlane(btmPlane, points.get(2));
        // AxisInfo l4 = getLineJointPlane(btmPlane, points.get(3));
        // 计算 B 点
        double disL = dis(l1, l2), disLB = this.slope.len / 2 - 57;
        double total = disL + disLB;
        // b1 - l2 / l1-l2 = total / disL
        AxisInfo c1 = add(multi(sub(l1, l2), (total / disL)), l2);
        // l1 - b2 / l1 - l2 = disL / total
        AxisInfo c2 = sub(l1, multi(sub(l1, l2), (disL / total)));
        AxisInfo c3 = add(l3, sub(c1, l1));
        AxisInfo c4 = get4thPoint(c1, c2, c3);

        {ret.add(c1); ret.add(c2); ret.add(c3); ret.add(c4);}
        return ret;
    }

    @Override
    public void chain(List<Double[]> refList) {
        U.print("init target C points...");
        calcPointC();
        U.print("init target C points done");

        U.print("init target E points ...");
        calcPointE(refList);
        U.print("init target E points done");
    }
}
