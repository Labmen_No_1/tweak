package com.jskj.fine_tune.tweak;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.jskj.fine_tune.tweak.U.f;

public final class Collector {
    private static final Collector _self = new Collector();
    private Map<String, Object> data;
    private Gson gson;

    private Collector() {
        data = new LinkedHashMap<>();
        gson = new GsonBuilder()
//                .setPrettyPrinting()
                .registerTypeAdapter(double.class, new DoubleAdapter())
                .create();
    }

    static class DoubleAdapter extends TypeAdapter<Double> {
        private static final DecimalFormat df = new DecimalFormat("#.#####");
        @Override
        public void write(JsonWriter out, Double value) throws IOException {
            String fmt = df.format(value);
            out.value(fmt);
        }

        @Override
        public Double read(JsonReader in) throws IOException {
            return BigDecimal.valueOf(in.nextDouble()).setScale(4, RoundingMode.HALF_UP).doubleValue();
        }
    }

    public static Collector get() {
        return _self;
    }

    public Collector add(String key, Object obj) {
        data.putIfAbsent(key, obj);
        return this;
    }

    public void rm() {
        this.data.clear();
    }

    public void dumpJsonFile(String fileName) {
        try (Writer writer = new FileWriter(f("dump/%s.json", fileName))) {
            gson.toJson(data, writer);
        } catch (Exception ex) {

        } finally {
            rm();
        }
    }
}
