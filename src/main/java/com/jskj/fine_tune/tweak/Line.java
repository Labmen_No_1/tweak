package com.jskj.fine_tune.tweak;

/**
 * 直线方程对象
 */
public class Line {

    /**
     * 直线上一点
     */
    AxisInfo point;

    /**
     * 方向向量
     */
    AxisInfo directionVec;

    public Line(AxisInfo point, AxisInfo directionVec) {
        this.point = point;
        this.directionVec = directionVec;
    }

    @Override
    public String toString() {
        return "Line{" +
                "point=" + point.toString() +
                ", directionVec=" + directionVec.toString() +
                '}';
    }

    public AxisInfo getDirectionVec() {
        return directionVec;
    }
}
