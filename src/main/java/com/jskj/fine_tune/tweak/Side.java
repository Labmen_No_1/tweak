package com.jskj.fine_tune.tweak;

public enum Side {
    /**
     * 板件短的一端调整
     */
    SHORT,
    /**
     * 板件长的一端调整
     */
    LONG
}