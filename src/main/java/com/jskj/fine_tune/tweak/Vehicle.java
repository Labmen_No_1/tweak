package com.jskj.fine_tune.tweak;

import java.util.List;

/**
 * 车的信息
 */
public class Vehicle {
    /**
     * 车的长宽高
     */
    double length, width, height;
    /**
     * 载具的大地座标系集合
     */
    List<AxisInfo> axisInfoList;

    /**
     * 垂直方向调整的4组千斤顶集合
     */
    List<Jack> vJacks;

    /**
     * 控制旋转的2个千斤顶
     */
    List<Jack> rJacks;

    /**
     * 控制平移的1组千斤顶
     */
    Jack hJack;

    /**
     * 获取载具的质心
     *
     * @return obj with x y z
     */
    AxisInfo getCenter() {
        return new AxisInfo(this.length / 2, this.width / 2, 0);
    }

    public Vehicle(double length, double width, double height) {
        this.length = length;
        this.width = width;
        this.height = height;
    }

    /**
     * 获取一个载具的默认值
     * @return Vehicle
     */
    public static Vehicle getDefault() {
        return new Vehicle(G.VEHICLE_LENGTH, G.VEHICLE_WIDTH, G.VEHICLE_HEIGHT);
    }
}
