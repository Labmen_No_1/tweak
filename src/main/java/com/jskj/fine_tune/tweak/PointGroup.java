package com.jskj.fine_tune.tweak;

import java.util.ArrayList;
import java.util.List;

import static com.jskj.fine_tune.tweak.G.*;
import static com.jskj.fine_tune.tweak.M.*;
import static com.jskj.fine_tune.tweak.U.print;

/**
 * 坐标点集合
 */
public abstract class PointGroup implements Group {

    protected Slope slope;

    protected Plane pointPlane;
    protected List<AxisInfo> points;

    /**
     * 板底
     */
    protected List<AxisInfo> cList;

    /**
     * 托盘
     */
    protected List<AxisInfo> eList;

    public PointGroup(Slope s, AxisInfo... points) {
        setParams(points);
        // 保存坡面参数
        slope = s;
    }

    protected void setParams(AxisInfo... points) {
        // 设置第四个点
        this.points = padPoint(points);
        // 计算平面
        pointPlane = planeFit(points);
    }

    protected abstract void calcPointC();

    protected abstract void chain(List<Double[]> refList);

    protected void calcPointE(List<Double[]> jackRefValList) {
        // 计算托盘和千斤顶在 RefX RefY 的差量
        double  deltaX = (V_BOARD_WIDTH - V_JACK_SHORT_DIS) / 2,
                deltaY = (V_BOARD_LENGTH - V_JACK_LONG_DIS) / 2;
        List<RefPoint> refList = new ArrayList<>();
        for (int i = 0, len = cList.size(); i < len; ++i) {
            AxisInfo c = cList.get(i);
            Double[] refArr = jackRefValList.get(i);
            double refX = refArr[0], refY = refArr[1];
            RefPoint refPoint = new RefPoint(c, (refX - deltaX), (refY - deltaY));
            refList.add(refPoint);
        }
        eList = calcPointByRef(refList, V_BOARD_WIDTH, V_BOARD_LENGTH);
    }

    /**
     * 计算需要依赖 C 点
     */
    protected List<AxisInfo> calcPointByRef(List<RefPoint> points, double shortSide, double longSide) {
        List<AxisInfo> ret = new ArrayList<>();
        RefPoint
                r1 = points.get(0),
                r2 = points.get(1),
                r3 = points.get(2),
                r4 = points.get(3);

        // 1 点
        // 求 1 点
        // 先求夹角用 m4-m3 的 refY 的分量
        double delta = r4.refY - r3.refY;
        if (delta == 0) {
            // 放平的情况
        }
        double disC1nC3 = -G.LASER_LENGTH;
        double disC3nC4 = -G.LASER_WIDTH;
        double rad = Math.asin(delta / disC3nC4);

        double c = delta / Math.cos(rad);
        double rate = disC1nC3 / c;
        // sub(c1, c3) / sub(x, c3) = rate
        AxisInfo p1 = add(r3, divide(sub(r1, r3), rate));

        // 2
        double disC4nP1 = disC3nC4 / Math.cos(rad);
        // 求短边
        double a = c * Math.sin(rad);
        rate = disC4nP1 / a;
        // sub(c4, p1) / sub(x, p1) = disC4nP1 / a
        AxisInfo p2 = add(p1, divide(sub(r4, p1), rate));

        // 3
        double disP3nP2 = r3.refX;
        rate = disP3nP2 / a;
        // sub(p3, p2) / sub(p1, p2) = rate
        AxisInfo p3 = add(p2, multi(sub(p1, p2), rate));

        // 4
        AxisInfo p4 = get4thPoint(p2, r3, p3);

        // 5
        double disP3nP4 = r3.refY - r4.refY;
        // m3 点的 refY
        double disP4nP5 = r3.refY;
        rate = disP4nP5 / disP3nP4;
        // sub(p5, p4) / sub(p3, p4) = disP4nP5 / disP3nP4
        AxisInfo p5 = add(p4, multi(sub(p3, p4), rate));

        // 6
        double disC4nP6 = -r4.refX;
        double disC4nP3 = -r4.refX - shortSide;
        rate = disC4nP6 / disC4nP3;
        // sub(p6, c4) / sub(p3, c4) = rate
        AxisInfo p6 = add(r4, multi(sub(p3, r4), rate));

        // 7
        AxisInfo p7 = get4thPoint(p3, p5, p6);

        // 8
        rate = disP4nP5 / -longSide;
        // sub(p5, p4) / sub(p5, p8) = rate
        AxisInfo p8 = sub(p5, divide(sub(p5, p4), rate));

        // p9
        AxisInfo p9 = sub(p8, sub(p5, p7));

        ret.add(p8);
        ret.add(p9);
        ret.add(p5);
        ret.add(p7);
        print("3 6距离 %.2f", dis(p3, p6));
        /*dumpPoints2Code("p", p1, p2, p3, p4, p5, p6, p7, p8, p9);*/
        return ret;
    }

    protected List<AxisInfo> commonStep(double height) {
        Plane underPlane = pointPlane.getUnderParallel(height);
        AxisInfo
                a1 = M.getLineJointPlane(underPlane, points.get(0)),
                a2 = M.getLineJointPlane(underPlane, points.get(1)),
                a3 = M.getLineJointPlane(underPlane, points.get(2));

        double disA1nA2 = M.dis(a1, a2), disA1nB1 = slope.calcDisAB();
        // 计算比例
        double rate = Math.abs(disA1nA2 / disA1nB1);
        AxisInfo x1 = sub(a1, divide(sub(a2, a1), rate));
        AxisInfo x2 = add(divide(sub(a2, a1), rate), a2);
        AxisInfo x3 = add(a3, sub(x1, a1));
        AxisInfo x4 = get4thPoint(x1, x2, x3);
        return new ArrayList<AxisInfo>(){{ add(x1);add(x2);add(x3);add(x4); }};
    }

    protected static class RefPoint extends AxisInfo {
        double refX, refY;

        public RefPoint(AxisInfo ax, double refX, double refY) {
            super(ax.x, ax.y, ax.z);
            this.refX = refX;
            this.refY = refY;
        }
    }

    protected void calcBoardNBtmAngle() {
        int numOne = 1, numTwo = 2;
        AxisInfo vec1 = sub(points.get(2), points.get(1));
        AxisInfo vec2 = sub(cList.get(2), cList.get(1));
        double angle = calcVecAngle(vec1, vec2);
        print("板和托盘的夹角为 %.5f", angle);
    }
}
