package com.jskj.fine_tune.tweak;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static com.jskj.fine_tune.tweak.G.*;
import static com.jskj.fine_tune.tweak.M.*;
import static com.jskj.fine_tune.tweak.U.print;
import static com.jskj.fine_tune.tweak.U.zip;

/**
 * 主入口
 */
public class Main {
    String taskName = "default";

    private MeasureGroup measureGroup;
    private TargetGroup targetGroup;

    private TikTokGroup shortGroup;
    private TikTokGroup longGroup;
    private ShiftGroup hShiftGroup;
    private ShiftGroup vShiftGroup;

    /**
     * 容忍误差
     */
    private double tolerate;
    private final Slope slope = Slope.getDefault();
    private TweakState state = new TweakState();

    private JackBrain jackBrain;

    private Collector collector;


    public Main(String name) {
        taskName = name;
    }

    public Main() {

    }

    /**
     * 计算入口
     * @return
     */
    public TweakResult fit() {
        // run check
        if (
                jackBrain == null ||
                measureGroup == null ||
                targetGroup == null ||
                shortGroup == null ||
                longGroup == null
        ) {
            throw new IllegalStateException("params not init !!!");
        }
        TweakResult resp = new TweakResult();
        try {
            print("num %d tweak", state.getNum());
            if (measureGroup.check(targetGroup.points, tolerate)) {
                resp.setCode(100);
                return resp;
            }
            init();
            // 根据 refX refY 分别推算测量与目标的坐标
            measureGroup.chain(state.getRefList());
            // 目标投影到测量上来
            List<AxisInfo> projPoints = projection(targetGroup.eList, measureGroup.eList);
            // 平移
            translate(measureGroup.eList, projPoints, resp);
            // 旋转
            rotate(measureGroup.eList, projPoints, resp);
            // 抬升
            raiseUp(measureGroup.eList, targetGroup.eList, resp);
            // getData();
            jackBrain.check(resp);
            // 计数
            state.pass();
        } catch (Exception ex) {
            resp.setCode(500);
            print("wrong! %s", ex.getMessage());
        }
        return resp;
    }

    private void getData() {
        collector.add("m", measureGroup.points);
        collector.add("me", measureGroup.eList);
        collector.add("mc", measureGroup.cList);
        collector.add("t", targetGroup.points);
        if (DUB_MOD) {
            // collector.dumpJsonFile(taskName);
        }
    }

    /**
     * 类初始化后只执行一次
     */
    private void init() {
        if (state.getNum() > 0) return;
        double vAngle = vShiftGroup.calcShiftAngle();
        double hAngle = hShiftGroup.calcShiftAngle();
        print("v angle %.2f h angle %.2f", vAngle, hAngle);
        {
            double totalRefY = G.LASER_LENGTH * Math.cos(Math.toRadians(vAngle));
            double totalRefX = G.LASER_WIDTH * Math.cos(Math.toRadians(hAngle));
            print(">>>> totalRefY %.2f totalRefX %.2f", totalRefX, totalRefY);
        }

        // 计算千斤顶的 ref 值
        double[] refX = longGroup.getRefVal();
        double[] refY = shortGroup.getRefVal();
        List<Double[]> refList = zip(refX, refY);
        reFix(refList);
        refList.forEach(elm -> print("refX %.2f refY %.2f", elm[0], elm[1]));
        // 存储到状态里面，整个调板过程会用到这组偏差量
        state.setRefList(refList);
        // 设置旋转初始角
        double oa = calcOriginalDeg();
        Stream.of("R1", "R2")
                .forEach(key -> state.jackRotateDeg.put(key, oa));
        // 数据收集器
        collector = Collector.get();
        // 目标 C E
        targetGroup.chain(refList);
    }

    private void reFix(List<Double[]> list) {
        Double[]
                one = list.get(0),
                two = list.get(1),
                three = list.get(2),
                four = list.get(3);
        double sum13RefY = one[1] + three[1];
        if (sum13RefY != REFY_MAX_VAL) {
            double rate = sum13RefY / REFY_MAX_VAL;
            one[1] /= rate;
            two[1] /= rate;
            three[1] /= rate;
            four[1] /= rate;
        }
        double sum12RefX = one[0] + two[0];
        double delta = sum12RefX / REFX_MAX_VAL;
        list.forEach(elm -> elm[0] /= delta);
        print("reFixed val %.2f", delta);
    }

    /**
     * 计算旋转初始角
     *
     * @return float64
     */
    private double calcOriginalDeg() {
        return toDeg(Math.acos((R_JACK_DIS / 2) / ROTATE_SPOT_DIS));
    }

    /**
     * 投影方法
     * @return
     */
    private List<AxisInfo> projection(List<AxisInfo> from, List<AxisInfo> to) {
        List<AxisInfo> ret = new ArrayList<>();
        Plane fromPlane = planeFit(from);
        Plane toPlane = planeFit(to);

        Line line = twoPlaneJoint(fromPlane, toPlane);
        // print("二面角：%.2f", twoPlaneAngle(fromPlane, toPlane));

        int len = from.size();

        for (int i = 0; i < len; i++) {
            AxisInfo fromPoint = from.get(i);
            AxisInfo projPoint;
            if (line != null) {
                AxisInfo footPoint = disBetweenPointAndLineFootPoint(fromPoint, line);
                projPoint = rotatePointByLine(fromPoint, footPoint, toPlane);
                 double sideLen = dis(footPoint, fromPoint);
                 // print("等腰三角形 腰长 %.5f 底边长 %.5f", sideLen, dis(fromPoint, projPoint));
            } else {
                // 测量与目标平行
                projPoint = getLineJointPlane(toPlane, fromPoint);
            }
            ret.add(projPoint);
        }
        return ret;
    }

    /**
     * 平移
     */
    private void translate(List<AxisInfo> measurePoints, List<AxisInfo> projPoints, TweakResult resp) {
        AxisInfo mCtr = getCenter(measurePoints);
        AxisInfo pCtr = getCenter(projPoints);

        AxisInfo vpm = sub(pCtr, mCtr);
        AxisInfo v31 = measureGroup.getJackVec(2, 0);

        // 计算平移夹角
        double angle = calcVecAngle(vpm, v31);

        // 计算出xy分量
        double dis = dis(mCtr, pCtr);
        double rad = Math.toRadians(angle);
        double trX = abs(dis * Math.sin(rad)),
                trY = abs(dis * Math.cos(rad));

        AxisInfo v21 = measureGroup.getJackVec(1, 0);

        double a1 = calcVecAngle0(vpm, v31);
        double a2 = calcVecAngle0(vpm, v21);
        int dir1 = a1 > 90 ? -1 : 1;
        int dir2 = a2 > 90 ? 1 : -1;

        double beta = Math.cos(Math.toRadians(state.getDeltaAngle()));
        double deltaR = dir1 * trX * beta;
        double deltaH = dir2 * trY;

        print("R1 R2 需要平移 %.2f H1 需要平移 %.2f", deltaR, deltaH);

        jackBrain.modify("R1", deltaR);
        jackBrain.modify("R2", deltaR);
        jackBrain.modify("H1", deltaH);

        // 这里需要存储 deltaR
        double oldVal = state.getDeltaR();
        state.setDeltaR(oldVal + deltaR);
    }

    public void rotate(List<AxisInfo> measurePoints, List<AxisInfo> projPoints, TweakResult resp) {
        int num2 = 1, num3 = 2;
        AxisInfo mVec = sub(measurePoints.get(num3), measurePoints.get(num2));
        AxisInfo rVec = sub(projPoints.get(num3), projPoints.get(num2));

        double angle = calcVecAngle(mVec, rVec);
        int dir = rotateDirection(mVec, rVec);
        print("夹角为 %.2f 目标旋转方向 %s时针", angle, dir == 1? "顺": "逆");

        // 如果为 0 代表没有倾斜, 不需要调整
        if (dir == 0) return;
        // 计算旋转后的千斤顶长度
        // 这个计算是根据需要增加行程的千斤顶计算的
        double r1Val = calcRotate((dir == -1 ? 1 : -1) * angle, "R1");
        double r2Val = calcRotate((dir == 1 ? 1 : -1) * angle, "R2");
        print(">>>> R1 curVal %.2f R2 curVal %.2f", r1Val, r2Val);
        jackBrain.setCurDis("R1", r1Val);
        jackBrain.setCurDis("R2", r2Val);
    }

    /**
     * 抬升
     */
    public void raiseUp(List<AxisInfo> measurePoints, List<AxisInfo> targetPoints, TweakResult resp) {
        // 先查看状态值里的千斤顶投影点
        List<AxisInfo> preJackProjPoints = state.getPreJackPos();
        // 把上次的千斤顶在托盘的位置, 重新投影到当前托盘面
        List<AxisInfo> curJackProjPoints = preJackProjPoints == null ?
                measureGroup.dList :
                projection(preJackProjPoints, measurePoints);
        // 再把当前的千斤顶位置投影到目标平面
        List<AxisInfo> jackPointOnTargetPlane = projection(curJackProjPoints, targetPoints);
        // 求出投影点与测量点的距离
        for (int i = 0, len = jackPointOnTargetPlane.size(); i < len; ++i) {
            String key = "V" + (i + 1);
            AxisInfo res = jackPointOnTargetPlane.get(i);
            AxisInfo cur = curJackProjPoints.get(i);

            double absVal = dis(res, cur);
            // 用 z 值比较判断抬升还是下降
            int dir = (res.z - cur.z) > 0 ? 1 : -1;
            jackBrain.modify(key, dir * absVal);
            print("%s 号抬高量 %.10f z delta %.2f", key, absVal, (targetGroup.points.get(i).z - measureGroup.points.get(i).z));
        }
        // 保存状态
        state.setPreJackPos(curJackProjPoints);
    }

    private double calcRotate(double rotateAngle, String key) {
        // 初始角度
        double originDeg = state.jackRotateDeg.get(key);
        // 一边的长度
        double shiftVal = state.getDeltaR();
        double aSide = CTR_JACK_BTM_DIS + shiftVal;
        double bSide = R_JACK_DIS / 2;

        double padDeg = toDeg(Math.atan(aSide / bSide));
        // pad side
        double a = Math.sqrt(aSide * aSide + bSide * bSide);


        double totalOrinAngle = (originDeg + rotateAngle);
        // 更新旋转角
        state.jackRotateDeg.put(key, totalOrinAngle);
        // 计算出总角度
        double totalDeg = totalOrinAngle + padDeg;
        // 电缸当前长度
        double b = ROTATE_SPOT_DIS;
        double c = get3rdSideLength(a, b, totalDeg);
        double curLen = c - R_JACK_LEN;

        //  alpha=acos((pow(b, 2)+pow(c, 2)-pow(a, 2))/(2*b*c))
        double beta = toDeg(Math.acos((a * a + c * c - b * b) / (2 * a * c)));
        // 偏斜角
        double rAngle = abs(90 - beta - padDeg);
        state.setDeltaAngle(rAngle);

        return curLen;
    }

    /**
     *
     * @return
     */
    public List<AxisInfo> show() {
        // TODO 城轨台 坡表表面中点
        return new ArrayList<>(measureGroup.bList);
    }

    public Main setTolerate(double val) {
        tolerate = val;
        return this;
    }

    public Main setTargetGroup(AxisInfo... points) {
        targetGroup = new TargetGroup(slope, points);
        return this;
    }

    public void setShortGroupBefore(double a, double b, AxisInfo... points) {
        shortGroup = new TikTokGroup(Side.SHORT);
        shortGroup.setBefore(a, b, points);
    }

    public void setShortGroupAfter(double a, double b, AxisInfo... points) {
        shortGroup.setAfter(a, b, points);
    }

    public void setLongGroupBefore(double a, double b, AxisInfo... points) {
        longGroup = new TikTokGroup(Side.LONG);
        longGroup.setBefore(a, b, points);
    }

    public void setHShiftGroupBefore(AxisInfo... points) {
        this.hShiftGroup = new ShiftGroup(ShiftGroup.Direction.HORIZON);
        hShiftGroup.setBeforePoints(points);
    }

    public void setHShiftGroupAfter(AxisInfo... points) {
        hShiftGroup.setAfterPoints(points);
    }

    public void setVShiftGroupBefore(AxisInfo... points) {
        this.vShiftGroup = new ShiftGroup(ShiftGroup.Direction.VERTICAL);
        vShiftGroup.setBeforePoints(points);
    }

    public void setVShiftGroupAfter(AxisInfo... points) {
        vShiftGroup.setAfterPoints(points);
    }

    public void setLongGroupAfter(double a, double b, AxisInfo... points) {
        longGroup.setAfter(a, b, points);
    }

    public Main saveJackInfo(Map<String, Double> params) {
        if (jackBrain == null) {
            jackBrain = new JackBrain(params);
        } else {
            jackBrain.updateJackDis(params);
        }
        return this;
    }

    public Main saveMeasurePoints(AxisInfo... points) {
        if (measureGroup == null) {
            measureGroup = new MeasureGroup(slope, points);
        } else {
            measureGroup.updateMeasurePoints(points);
        }
        return this;
    }

    private final int jackDirection(double angle) {
        if (angle > 90) return -1;
        else if (angle < 90) return 1;
        else return 0;
    }

}
