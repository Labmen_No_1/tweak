package com.jskj.fine_tune.tweak;

public class Plane {
    double a, b, c, d;

    /**
     * 平面上一点
     */
    AxisInfo point;

    public Plane(double a, double b, double c, double d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    /**
     * 检查法向量的乘积正负性
     *
     * @return bool
     */
    boolean checkNorm() {
        return (a * b * c) > 0;
    }

    /**
     * 获取一个平行平面
     * @param delta 距离
     * @return
     */
    Plane getUnderParallel(double delta) {
        double e = d - delta * M.norm(a, b, c);
        return new Plane(a, b, c, e);
    }

    /**
     * 获取法向量
     * @return obj
     */
    public AxisInfo getNormalVec() {
        return new AxisInfo(a, b, c);
    }



    @Override
    public String toString() {
        return "Plane{" +
                "a=" +   String.format("%.10f", a) +
                ", b=" + String.format("%.10f", b) +
                ", c=" + String.format("%.10f", c) +
                ", d=" + String.format("%.10f", d) +
                '}';
    }

    public double[] getNormalVec3D() {
        AxisInfo normVec = getNormalVec();
        return new double[]{normVec.x, normVec.y, normVec.z};
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    public double getD() {
        return d;
    }

    public void setD(double d) {
        this.d = d;
    }
}
