package com.jskj.fine_tune.tweak;

import static com.jskj.fine_tune.tweak.U.print;

/**
 * 坡面（承轨台）对象
 */
public class Slope {
    double len, leftHeight, rightHeight, laserLen;

    public Slope(double len, double leftHeight, double rightHeight, double laserLen) {
        this.len = len;
        this.leftHeight = leftHeight;
        this.rightHeight = rightHeight;
        this.laserLen = laserLen;
    }

    /**
     * 破面斜率
     */
    double rate() {
        return (this.leftHeight - this.rightHeight) / this.len;
    }

    /**
     * 计算杆顶点距离板的上表面的垂直距离
     * @return float64
     */
    double calcHeight() {
        double rate = rate();
        // 1. 计算倾斜角弧度
        double rad = Math.atan(rate);
        // 2.
        double h1 = (this.leftHeight - this.rightHeight);
        double h2 = (this.len - 365 - 70) * rate;
        double deltaH1 = h1 - h2;
        // 3.
        double b1 = this.laserLen * Math.tan(rad);
        double h3 = b1 * Math.sin(rad);
        // 4.
        double h4 = deltaH1 - h3;
        // 5
        double h5 = this.laserLen / Math.cos(rad);
        return (h4 + h5 + this.rightHeight);
    }

    /**
     *
     * @return
     */
    double calcDisAB() {
        return Math.sin(Math.atan(rate())) * this.laserLen;
    }

    /**
     * 获取一个默认值
     * @return Slope
     */
    public static Slope getDefault() {
        return new Slope(492, 36, 23.7, G.LASER_LEN);
    }
}
