package com.jskj.fine_tune.tweak;

/**
 * 测量点信息
 */
public class MeasurePoint extends AxisInfo {

    /**
     * 棱镜编号
     * 1 base
     * 只需要设置一次
     */
    int num;

    /**
     * 板的绝对座标
     * 初次加载
     */
    AxisInfo vBoardCoord;

    /**
     * 承轨台表面辅助计算B点的坐标信息
     * 用于展示
     * 每次重新计算
     */
    AxisInfo pointA;

    /**
     * 承轨台表面的坐标信息
     * 用于展示
     * 每次重新计算
     */
    AxisInfo pointB;

    /**
     * 承轨台底面的坐标信息
     * 每次重新计算
     */
    AxisInfo pointC;

    /**
     * 千斤顶中心的绝对坐标
     */
    AxisInfo pointD;

    /**
     * 托盘顶点的绝对坐标
     */
    AxisInfo pointE;

    /**
     * 相对坐标系下测量点相对于板顶点的横纵偏差
     */
    AxisInfo boardDiff;

    /**
     * 相对坐标系下测量点相对于千斤顶中心的偏差
     */
    AxisInfo jackDiff;

    /**
     * 对应的目标点
     * 只需要设置一次
     */
    TargetPoint targetPoint;

    /**
     * 调整后的坐标
     * 这其实是个纯废物属性
     * 因为需要两次抖动抬升，需要存储每次抬升计算后的变量，
     * 之后的再调整用不到
     */
    double curX, curY, curZ;

    /**
     * 相对与车板的坐标
     */
    double refX, refY;

    /**
     * 计算抬升后 z 轴高度差
     */
    public double calcDeltaVal() {
        U.print(U.f("[debug] %.2f - %.2f", this.curZ, this.z));
        return this.curZ - this.z;
    }

    public void setCurrentAxisInfo(double... arr) {
        this.curX = arr[0];
        this.curY = arr[1];
        this.curZ = arr[2];
    }

    public MeasurePoint(double x, double y, double z) {
        super(x , y, z);
    }

    /**
     * 变成大野爹
     * @return
     */
    public AxisInfo toSuper() {
        return new AxisInfo(x, y, z);
    }


    /**
     * 计算对应车板的绝对坐标
     */
    void calcVehicleBoardAxisInfo(AxisInfo diff) {
        // 用该点的相对坐标与车板顶点的相对坐标的差值推算
        pointD = M.add(pointC, diff);
    }

    /**
     * 与目标检查
     * @return
     * true 完成
     * false 未完成
     */
    boolean isDone(double tolerance) {
        double deltaX = Math.abs(targetPoint.x - this.pointB.x);
        double deltaY = Math.abs(targetPoint.y - this.pointB.y);
        double deltaZ = Math.abs(targetPoint.z - this.pointB.z);

        return deltaX <= tolerance && deltaY <= tolerance && deltaZ <= tolerance;
    }

    /**
     * 是否求 D 点
     * @param isD true 是 false E
     */
    public void changeRef(boolean isD) {
        AxisInfo diff = isD ? jackDiff : boardDiff;
        this.refX = diff.x;
        this.refY = diff.y;
    }

    /**
     * 从外部获取的坐标，单位通常为 m
     * @return
     */
    public static MeasurePoint fromMachine(double x, double y, double z) {
        return new MeasurePoint(1000 * x, 1000 * y, 1000 * z);
    }

    public static MeasurePoint fromAxisInfo(AxisInfo ax) {
        return new MeasurePoint(ax.x, ax.y, ax.z);
    }

    /**
     * 比较转化后的点和冷静测量值的比率
     */
    public void printConvertRate() {
        // 测量值 xyz
        double rateBXY = (pointA.x - x) / (pointA.y - y);
        double rateBXZ = (pointA.x - x) / (pointA.z - z);
        double rateBYZ = (pointA.y - y) / (pointA.z - z);
        U.print(U.f("rateAoXY %.10f rateAoXZ %.10f rateAoYZ %.10f", rateBXY, rateBXZ, rateBYZ));
        double rateCXY = (pointC.x - pointB.x) / (pointC.y - pointB.y);
        double rateCXZ = (pointC.x - pointB.x) / (pointC.z - pointB.z);
        double rateCYZ = (pointC.y - pointB.y) / (pointC.z - pointB.z);

        U.print(U.f("rateBCXY %.10f rateBCXZ %.10f rateBCYZ %.10f", rateCXY, rateCXZ, rateCYZ));
    }

    public AxisInfo toAxisInfo() {
        return new AxisInfo(x, y, z);
    }
}
