package com.jskj.fine_tune.tweak;

/**
 * 千斤顶类型枚举
 */
public enum JackType {

    /**
     * 控制抬升 4 组
     */
    VERTICAL("V"),
    /**
     * 控制旋转以及平移 2 组
     */
    ROTATE("R"),
    /**
     * 控制左右方向移动 1 组
     */
    HORIZON("H");

    /**
     * 别名简称
     */
    final String alias;

    JackType(String alias) {
        this.alias = alias;
    }
}
