package com.jskj.fine_tune.tweak;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用来存储调板状态的类
 */
public class TweakState {
    /**
     * 调整次数
     * 初始值为 1
     */
    private int num = 0;
    /**
     * 托盘偏转角度
     * 初始状态是 0 度
     */
    private double degree = 0;

    private List<Double[]> refList = new ArrayList<>();

    /**
     * 千斤顶偏转角，会影响平移
     */
    private double deltaAngle = 0d;

    /**
     * 旋转中心相对于中点的偏移量
     */
    private double deltaR = 0d;

    /**
     * 上一次调板
     * 千斤顶在托盘平面的坐标
     */
    private List<AxisInfo> preJackPos;

    /**
     * 用来记录控制旋转的两个千斤顶的旋转变量角度
     */
    public Map<String, Double> jackRotateDeg = new HashMap<>(4);

    public void dumpStateInfo() {
        U.print("deltaAngle %.2f", deltaAngle);
        U.print("pre jack pos:");
        preJackPos.forEach(elm -> U.print("%s", elm.toString()));
        U.print("R1 current Angle %.2f", jackRotateDeg.get("R1"));
        U.print("R2 current Angle %.2f", jackRotateDeg.get("R2"));
    }


    public void pass() {
        num += 1;
        dumpStateInfo();
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public double getDegree() {
        return degree;
    }

    public void setDegree(double degree) {
        this.degree = degree;
    }

    public List<Double[]> getRefList() {
        return refList;
    }

    void setRefList(List<Double[]> refList) {
        this.refList = refList;
    }

    public double getDeltaAngle() {
        return deltaAngle;
    }

    public void setDeltaAngle(double deltaAngle) {
        this.deltaAngle = deltaAngle;
    }

    public double getDeltaR() {
        return deltaR;
    }

    public void setDeltaR(double deltaR) {
        this.deltaR = deltaR;
    }

    public List<AxisInfo> getPreJackPos() {
        return preJackPos;
    }

    public void setPreJackPos(List<AxisInfo> preJackPos) {
        this.preJackPos = preJackPos;
    }

}
