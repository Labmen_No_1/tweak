package com.jskj.fine_tune.tweak;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 精调结果
 */
public class TweakResult {
    /**
     * 每个千斤顶的调整量
     * key 千斤顶简称+编号 例如 R0 V1 H0
     * val 调整量 0 代表不调整
     * 正数 V 代表向上抬升 负数代表下降
     * 正数 H 代表向右移动 负数代表向左移动
     * 正数 R 代表向上移动 负数代表向下移动
     *
     * 按顺序遍历Map依次调整
     */
    private Map<String, Double> jackTweakMap = new LinkedHashMap<>();

    /**
     * 200 代表车就位并且调整成功
     * 400 代表需要移动拖车
     * 300 用来展示偏移量
     */
    private int code;

    /**
     * 车的水平调整量
     * 正数代表向右移动 负数代表向左移动
     */
    private double horizonShift;

    /**
     * 垂直方向的位移
     * 正数代表向上移动 负数代表向下移动
     */
    private double verticalShift;

    /**
     * 用来展示板件的便宜情况
     */
    private List<AxisInfo> pointShiftInfoList = new ArrayList<>();

    public TweakResult(Map<String, Double> jackTweakMap, int code) {
        this.jackTweakMap = jackTweakMap;
        this.code = code;
    }

    public TweakResult() {
    }

    public TweakResult(int code, double horizonShift, double verticalShift) {
        this.code = code;
        this.horizonShift = horizonShift;
        this.verticalShift = verticalShift;
    }

    public TweakResult(int code, List<AxisInfo> pointShiftInfoList) {
        this.code = code;
        this.pointShiftInfoList = pointShiftInfoList;
    }

    public Map<String, Double> getJackTweakMap() {
        return jackTweakMap;
    }

    public int getCode() {
        return code;
    }

    public double getHorizonShift() {
        return horizonShift;
    }

    public double getVerticalShift() {
        return verticalShift;
    }

    public List<AxisInfo> getPointShiftInfoList() {
        return pointShiftInfoList;
    }

    public void setJackTweakMap(Map<String, Double> jackTweakMap) {
        this.jackTweakMap = jackTweakMap;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setHorizonShift(double horizonShift) {
        this.horizonShift = horizonShift;
    }

    public void setVerticalShift(double verticalShift) {
        this.verticalShift = verticalShift;
    }

    public void setPointShiftInfoList(List<AxisInfo> pointShiftInfoList) {
        this.pointShiftInfoList = pointShiftInfoList;
    }
}
