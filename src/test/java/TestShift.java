import com.jskj.fine_tune.tweak.*;
import org.junit.Before;
import org.junit.Test;

public class TestShift {


    AxisInfo[] hb = new AxisInfo[]{
            AxisInfo.of(6.4180,1.7943,-0.6876),
            AxisInfo.of(4.5608,1.5769,-0.6851),
            AxisInfo.of(6.9012,-2.3416,-0.6839),
            AxisInfo.of(5.0447,-2.5598,-0.6953),
    };
    AxisInfo[] ha = new AxisInfo[]{
            AxisInfo.of(6.4241,1.7452,-0.6875),
            AxisInfo.of(4.5671,1.5277,-0.6851),
            AxisInfo.of(6.9068,-2.3908,-0.6841),
            AxisInfo.of(5.0505,-2.6093,-0.6954),
    };
    AxisInfo[] rb = new AxisInfo[]{
            AxisInfo.of(6.4179,1.7945,-0.6877),
            AxisInfo.of(4.5624,1.5767,-0.6852),
            AxisInfo.of(6.9019,-2.3415,-0.6842),
            AxisInfo.of(5.0462,-2.5601,-0.6955),
    };
    AxisInfo[] ra = new AxisInfo[]{
            AxisInfo.of(6.4663,1.8003,-0.6874),
            AxisInfo.of(4.6101,1.5828,-0.6848),
            AxisInfo.of(6.9506,-2.3355,-0.6841),
            AxisInfo.of(5.0934,-2.5541,-0.6952),
    };

    @Before
    public void setup() {

    }


    @Test
    public void calcRefTotal() {
        // b a
        AxisInfo hv0 = M.sub(hb[0], ha[0]);
        // 13
        AxisInfo hv1 = M.sub(ha[0], ha[2]);

        // 12
        AxisInfo rv2 = M.sub(ra[1], ra[0]);
        // b a
        AxisInfo rv0 = M.sub(rb[0], ra[0]);

        double angle1 = M.calcVecAngle0(hv0, hv1);
        double totalRefY = G.LASER_LENGTH * Math.cos(Math.toRadians(angle1));

        double angle2 = M.calcVecAngle0(rv0, rv2);
        double totalRefX = G.LASER_WIDTH * Math.cos(Math.toRadians(angle2));

        System.out.println(angle1);
        System.out.println(angle2);
        System.out.println(totalRefY);
        System.out.println(totalRefX);

    }

    @Test
    public void calcRefTotal0() {
        // b a
        AxisInfo hv0 = M.sub(rb[0], ra[0]);
        // 13
        AxisInfo hv1 = M.sub(ra[0], ra[2]);

        // 12
        AxisInfo hv2 = M.sub(ra[0], ra[1]);

        double angle1 = M.calcVecAngle0(hv0, hv1);
        double totalRefY = G.LASER_LENGTH * Math.cos(Math.toRadians(angle1));

        double angle2 = M.calcVecAngle0(hv0, hv2);
        double totalRefX = G.LASER_WIDTH * Math.cos(Math.toRadians(angle2));

        System.out.println(totalRefX);
        System.out.println(totalRefY);

    }
}
