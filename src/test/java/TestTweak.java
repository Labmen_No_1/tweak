import com.jskj.fine_tune.tweak.Line;
import com.jskj.fine_tune.tweak.Plane;
import com.jskj.fine_tune.tweak.*;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.stream.Stream;

import static com.jskj.fine_tune.tweak.M.*;

public class TestTweak {

    MeasurePoint[] measurePoints = new MeasurePoint[3];
    MeasurePoint[] measurePointsLong = new MeasurePoint[3];
    MeasurePoint[] measurePointsShort = new MeasurePoint[3];
    TargetPoint[] targetPoints = new TargetPoint[3];

    double raiseUpVal;

    double shortRaiseVal;
    double longRaiseVal;

    private void grp01() {
        shortRaiseVal = 18.6;
        longRaiseVal = 19.95;
        measurePoints[0] = MeasurePoint.fromMachine(6.5081, 2.5076 ,-0.6876);
        measurePoints[1] = MeasurePoint.fromMachine(4.6467, -0.6773,-0.6773);
        measurePoints[2] = MeasurePoint.fromMachine(6.9585, -1.634 ,-0.6934);

        measurePointsLong[0] = MeasurePoint.fromMachine(6.496 , 2.5065,-0.6616);
        measurePointsLong[1] = MeasurePoint.fromMachine(4.6351, 2.3043,-0.6795);
        measurePointsLong[2] = MeasurePoint.fromMachine(6.9464,-1.635 , -0.667);
//        measurePointsLong[3] = MeasurePoint.fromMachine(38176.8214,42673.5377,-15.6151);

        measurePointsShort[0] = MeasurePoint.fromMachine(6.5074, 2.5133 ,-0.6949);
        measurePointsShort[1] = MeasurePoint.fromMachine(4.6465, 2.3117 ,-0.6853);
        measurePointsShort[2] = MeasurePoint.fromMachine(6.9572, -1.6278,-0.6661);
//        measurePointsShort[3] = MeasurePoint.fromMachine(38176.8224,42673.5503,-15.5876);

        targetPoints[0] = TargetPoint.fromMachine(6.4805, 2.5016 ,-0.6861);
        targetPoints[1] = TargetPoint.fromMachine(4.6225, 2.2727 ,-0.6788);
        targetPoints[2] = TargetPoint.fromMachine(6.9902, -1.6331,-0.6805);
//        targetPoints[3] = new TargetPoint(38176.8214,42673.5377,-15.6151);
    }

    private void grp03() {
        shortRaiseVal = 18.55;
        longRaiseVal = 17.4;
        measurePoints[0] = MeasurePoint.fromMachine(8.1280,-0.7159,       -0.6980);
        measurePoints[1] = MeasurePoint.fromMachine(7.1750,0.8952,        -0.6884);
        measurePoints[2] = MeasurePoint.fromMachine(4.5427,-2.8374,       -0.7035 );

        measurePointsLong[0] = MeasurePoint.fromMachine(8.1233, -0.7063,       -0.6775);
        measurePointsLong[1] = MeasurePoint.fromMachine(7.1701, 0.9055,        -0.6935);
        measurePointsLong[2] = MeasurePoint.fromMachine(4.5377, -2.8272,       -0.6775);
//        measurePointsLong[3] = MeasurePoint.fromMachine(38176.8214,42673.5377,-15.6151);

        measurePointsShort[0] = MeasurePoint.fromMachine(8.1330, -0.7132,       -0.7056);
        measurePointsShort[1] = MeasurePoint.fromMachine(7.1799, 0.8982,        -0.6947);
        measurePointsShort[2] = MeasurePoint.fromMachine(4.5486, -2.8361,       -0.6816);
//        measurePointsShort[3] = MeasurePoint.fromMachine(38176.8224,42673.5503,-15.5876);

        targetPoints[0] = TargetPoint.fromMachine(8.0990, -0.7297,       -0.6966);
        targetPoints[1] = TargetPoint.fromMachine(7.1529, 0.8858,        -0.6918);
        targetPoints[2] = TargetPoint.fromMachine(4.5043, -2.8352,       -0.6903);
//        targetPoints[3] = new TargetPoint(38176.8214,42673.5377,-15.6151);
    }

    private void old() {
        shortRaiseVal = 19.1;
        longRaiseVal = 19.1;
        measurePoints[0] = MeasurePoint.fromMachine(38174.2677,42677.3300,-15.6057);
        measurePoints[1] = MeasurePoint.fromMachine(38173.2832,42675.7370,-15.5952);
        measurePoints[2] = MeasurePoint.fromMachine(38177.8120,42675.1409,-15.6104);

        measurePoints[0] = MeasurePoint.fromMachine(38174.2611,42677.3195,-15.5807);
        measurePoints[1] = MeasurePoint.fromMachine(38173.2771,42675.7266,-15.5976);
        measurePoints[2] = MeasurePoint.fromMachine(38177.8058,42675.1307,-15.5860);
//        measurePoints[3] = MeasurePoint.fromMachine(38176.8275,42673.5476,-15.6119 );

        measurePointsLong[0] = MeasurePoint.fromMachine(38174.2611,42677.3195,-15.5807);
        measurePointsLong[1] = MeasurePoint.fromMachine(38173.2771,42675.7266,-15.5976);
        measurePointsLong[2] = MeasurePoint.fromMachine(38177.8058,42675.1307,-15.5860);
//        measurePointsLong[3] = MeasurePoint.fromMachine(38176.8214,42673.5377,-15.6151);

        measurePointsShort[0] = MeasurePoint.fromMachine(38174.2622,42677.3325,-15.6127);
        measurePointsShort[1] = MeasurePoint.fromMachine(38173.2779,42675.7397,-15.6028);
        measurePointsShort[2] = MeasurePoint.fromMachine(38177.8068,42675.1436,-15.5853);
//        measurePointsShort[3] = MeasurePoint.fromMachine(38176.8224,42673.5503,-15.5876);

        targetPoints[0] = TargetPoint.fromMachine(38174.2611,42677.3195,-15.5807);
        targetPoints[1] = TargetPoint.fromMachine(38173.2771,42675.7266,-15.5976);
        targetPoints[2] = TargetPoint.fromMachine(38177.8058,42675.1307,-15.5860);
//        targetPoints[3] = new TargetPoint(38176.8214,42673.5377,-15.6151);
    }

    private void yestd() {
        measurePoints[0] = MeasurePoint.fromMachine(6.5081,2.5076,-0.6876);
        measurePoints[1] = MeasurePoint.fromMachine(4.6467,2.3055,-0.6773);
        measurePoints[2] = MeasurePoint.fromMachine(6.9585,-1.634,-0.6934);
//        measurePoints[3] = new MeasurePoint(5.0964,-1.8359,-0.6951 );

        measurePointsLong[0] =  MeasurePoint.fromMachine(6.496,2.5065,-0.6616);
        measurePointsLong[1] =  MeasurePoint.fromMachine(4.6351,2.3043,-0.6795);
        measurePointsLong[2] =  MeasurePoint.fromMachine(6.9464,-1.635,-0.667);
//        measurePointsLong[3] ew MeasurePoint(5.0845,-1.8371,-0.697);

        measurePointsShort[0] = MeasurePoint.fromMachine(6.5074,2.5133,-0.6949);
        measurePointsShort[1] = MeasurePoint.fromMachine(4.6465,2.3117,-0.6853);
        measurePointsShort[2] = MeasurePoint.fromMachine(6.9572,-1.6278,-0.6661);
//        measurePointsShort[3] = new MeasurePoint(5.0952,-1.8299,-0.6686);

        targetPoints[0] = TargetPoint.fromMachine(6.4805, 2.5016, -0.6861);
        targetPoints[1] = TargetPoint.fromMachine(4.6225, 2.2727, -0.6788);
        targetPoints[2] = TargetPoint.fromMachine(6.9902, -1.6331,-0.6805);
//        targetPoints[3] = new TargetPoint(5.1314,-1.8619,-0.6853);
    }

    private void noRaiseUp() {
        shortRaiseVal = 30;
        longRaiseVal = 30;

        measurePoints[0] = MeasurePoint.fromMachine(38174.2677,42677.3300,-15.6057);
        measurePoints[1] = MeasurePoint.fromMachine(38173.2832,42675.7370,-15.5952);
        measurePoints[2] = MeasurePoint.fromMachine(38177.8120,42675.1409,-15.6104);
//        measurePoints[3] = new MeasurePoint(5.0964,-1.8359,-0.6951 );

        measurePointsLong[0] =  MeasurePoint.fromMachine(38174.2611,42677.3195,-15.5807);
        measurePointsLong[1] =  MeasurePoint.fromMachine(38173.2771,42675.7266,-15.5976);
        measurePointsLong[2] =  MeasurePoint.fromMachine(38177.8058,42675.1307,-15.5860);
//        measurePointsLong[3] ew MeasurePoint(5.0845,-1.8371,-0.697);

        measurePointsShort[0] = MeasurePoint.fromMachine(38174.2622,42677.3325,-15.6127);
        measurePointsShort[1] = MeasurePoint.fromMachine(38173.2779,42675.7397,-15.6028);
        measurePointsShort[2] = MeasurePoint.fromMachine(38177.8068,42675.1436,-15.5853);
//        measurePointsShort[3] = new MeasurePoint(5.0952,-1.8299,-0.6686);

        targetPoints[0] = TargetPoint.fromMachine(38174.3292,42677.3331,-15.6060);
        targetPoints[1] = TargetPoint.fromMachine(38173.3131,42675.7604,-15.5961);
        targetPoints[2] = TargetPoint.fromMachine(38177.8288,42675.0729,-15.6106);
    }

    private void tdy() {
        shortRaiseVal = 18.55;
        longRaiseVal = 17.4;
        measurePoints[0] = MeasurePoint.fromMachine(8.1280 ,-0.7159,       -0.6980);
        measurePoints[1] = MeasurePoint.fromMachine(7.1750 ,0.8952,        -0.6884);
        measurePoints[2] = MeasurePoint.fromMachine(4.5427 ,-2.8374,       -0.7035 );
//        measurePoints[3] = new MeasurePoint(5.0964,-1.8359,-0.6951 );

        measurePointsLong[0] =  MeasurePoint.fromMachine(8.1233, -0.7063,      -0.6775);
        measurePointsLong[1] =  MeasurePoint.fromMachine(7.1701, 0.9055 ,     -0.6935);
        measurePointsLong[2] =  MeasurePoint.fromMachine(4.5377, -2.8272,      -0.6775);
//        measurePointsLong[3] ew MeasurePoint(5.0845,-1.8371,-0.697);

        measurePointsShort[0] = MeasurePoint.fromMachine(8.1330, -0.7132,       -0.7056);
        measurePointsShort[1] = MeasurePoint.fromMachine(7.1799, 0.8982, -0.6947);
        measurePointsShort[2] = MeasurePoint.fromMachine(4.5486, -2.8361,       -0.6816);
//        measurePointsShort[3] = new MeasurePoint(5.0952,-1.8299,-0.6686);

        targetPoints[0] = TargetPoint.fromMachine(8.1234, -0.7142,-0.6767);
        targetPoints[1] = TargetPoint.fromMachine(7.1710 ,-0.6042,-0.6733);
        targetPoints[2] = TargetPoint.fromMachine(4.5393 ,-2.8371,-0.7013);
//        targetPoints[3] = new TargetPoint(5.1314,-1.8619,-0.6853);
    }
    @Before
    public void setUp() {
        grp01();
    }

    @Test
    public void foo() {
        Controller controller = new Controller.Builder()
                // 设置容忍误差
                .setTolerate(3)
                // 设置目标位置 3 个点位 xyz
                // 承轨台上表面的坐标
                .setTargetPointArr(
                        targetPoints
                )
                // 设置垂直4组千斤顶
                .setVJackArr(
                        // 分别是 千斤顶类型 初始行程 千斤顶长度（V 类型可以设置为0） 最大行程 安装位置xy（相对于车）
                        new Jack(JackType.VERTICAL, 75, 0, 150, 450, 125),
                        new Jack(JackType.VERTICAL, 75, 0, 150, 2750, 125),
                        new Jack(JackType.VERTICAL, 75, 0, 150, 450, 1425),
                        new Jack(JackType.VERTICAL, 75, 0, 150, 2750, 1425)
                )
                // 设置控制旋转的2组千斤顶
                .setRJackArr(
                        new Jack(JackType.ROTATE, 60, 100, 200, 400, 402),
                        new Jack(JackType.ROTATE, 60, 100, 200, 2800, 402)
                )
                // 设置水平位移的千斤顶
                .setHJackArr(
                        new Jack(JackType.HORIZON, 100, 60, 200, 230, 450)
                )
                // 设置初始抬高量
                .setRaiseUpVal(raiseUpVal)
                .build();


        // 设置测量坐标点
        controller.setMeasurePoints(
                measurePoints
        );

        /**
         * 一共 7 组
         */
        HashMap<String, Double> jackMap = new HashMap<>(8, 1){{
            put("V1", 60.);
            put("V2", 60.);
            put("V3", 60.);
            put("V4", 60.);
            put("R1", 100.);
            put("R2", 100.);
            put("H1", 100.);
        }};
        controller.updateJackDistance(jackMap);

        controller.raiseUp(Side.SHORT, shortRaiseVal).updateMeasurePoint(
                measurePointsShort
        );
        // 之后再复位
        // reset
        controller.raiseUp(Side.LONG, longRaiseVal).updateMeasurePoint(
                measurePointsLong
        );
        // 之后再复位
        // reset
        /*-------------------------抖动---------------------------*/

        // 调整
        TweakResult result = controller.fit();
        // 用 code 判断是否需要调整车
        // 200 不需要 400 需要 300 展示信息
        int code = result.getCode();
        if (code == 200) {
            // map key 是千斤顶编号 H1 val 是调整量 -10 10
            Map<String, Double> jackTweakMap = result.getJackTweakMap();
            // 4个点存储
            // 棱镜转化到板*上表面*的座标
            List<AxisInfo> points = controller.show();
        } else if (code == 400) {
            // 调整车的数据, h 水平位移 v 垂直位移 -+
            double h = result.getHorizonShift(),
                    v = result.getVerticalShift();
        } else if (code == 100) {
            // 比较4个点与目标点的差值
            List<AxisInfo> points = controller.show();
            System.err.println("done");
        } else {
            System.out.println("error");
        }
        //
    }


    private double calcRealHeight(double slopeHeight, double slopeLength, double laserHeight) {
        // 计算为弧度
        double rad = Math.atan(slopeHeight / slopeLength);
        double c = laserHeight / Math.cos(rad);
        double delta = laserHeight * Math.tan(rad) * Math.sin(rad);
        return (slopeHeight / 2 - delta + c);
    }

    @Test
    public void testSend() {
        for (double j : Arrays.asList(G.V_BOARD_INSTALL_Y, (G.VEHICLE_WIDTH - G.V_BOARD_INSTALL_Y)))
            for (double i : Arrays.asList(G.V_BOARD_INSTALL_X, (G.VEHICLE_LENGTH - G.V_BOARD_INSTALL_X)))
                System.out.println(i + ", " + j);
    }

    @Test
    public void test4thPoint() {
        AxisInfo thPoint = M.get4thPoint(this.measurePoints);
        System.out.println(thPoint);
    }


    @Test
    public void testRotateAngle() {
        AxisInfo a = AxisInfo.of(42674.9005, 38181.2688, -15.9289);
        AxisInfo b = AxisInfo.of(42677.057, 38179.7047, -15.9765);

        AxisInfo c = AxisInfo.of(42674.8389, 38181.1759, -15.9275);
        AxisInfo d = AxisInfo.of(42677.1178, 38179.7961, -15.9781);

        AxisInfo vecA = M.sub(a, b);
        AxisInfo vecB = M.sub(c, d);
        System.out.println(vecA);
        System.out.println(vecB);
        double v = M.calcVecAngle(vecA, vecB);
        System.out.println(v);
    }

    @Test
    public void testLineJointPlane() {
        Plane plane = new Plane(0.014518980000025893, 0.029969160000000515, -6.18035166001452, -3969.9969877338153);
        AxisInfo point = AxisInfo.of(38189.6593, 42663.4889, -15.7576);
        AxisInfo lineJointPlane = M.getLineJointPlane(plane, point);
        System.out.println(lineJointPlane.toString());
    }

    @Test
    public void testRotate() {
        // 托盘
        AxisInfo p1 = AxisInfo.of(38187209.21, 42664266.50, -14970.53);
        AxisInfo p2 = AxisInfo.of(38187829.75, 42665463.33, -14960.44);
        AxisInfo p3 = AxisInfo.of(38184730.27, 42665786.04, -14963.82);
        AxisInfo p4 = AxisInfo.of(38185350.80, 42666982.87, -14953.72);

        // 板底
        AxisInfo t1 = AxisInfo.of(38187754.69,42663724.17, -14973.57);
        AxisInfo t2 = AxisInfo.of(38188682.32,42665363.37, -14959.59);
        AxisInfo t3 = AxisInfo.of(38184126.89,42665769.47, -14965.07);
        AxisInfo t4 = AxisInfo.of(38185054.52,42667408.67, -14951.09);

        System.out.println(Math.sqrt(291.33 * 291.33 + 719.67 * 719.67));
        System.out.println(M.dis(p1, t1));

        System.out.println("间距");
        System.out.println(M.dis(p1, p2));
        System.out.println(M.dis(t1, t2));

        System.out.println("13");
        System.out.println(M.dis(p1, p3));
        System.out.println(M.dis(t1, t3));

        System.out.println("36 dis");

        System.out.println("t3t4");
        System.out.println(M.dis(t3, t4));
    }

    @Test
    public void testShortAndLong() {
        AxisInfo s1 = AxisInfo.of(38187766.30, 42663728.60, -15546.60);
        AxisInfo s2 = AxisInfo.of(38188689.10, 42665359.20, -15533.80);
        AxisInfo s3 = AxisInfo.of(38184138.20, 42665773.40, -15484.00);
        AxisInfo s4 = AxisInfo.of(38185054.80, 42667409.20, -15482.50);

        AxisInfo l1 = AxisInfo.of(38187767.50, 42663748.40, -15496.00);
        AxisInfo l2 = AxisInfo.of(38188690.00, 42665379.00, -15525.90);
        AxisInfo l3 = AxisInfo.of(38184139.00, 42665792.80, -15486.60);
        AxisInfo l4 = AxisInfo.of(38185055.00, 42667429.80, -15527.80);

        System.out.println("12");
        System.out.println(M.dis(s1, s2));
        System.out.println(M.dis(l1, l2));

        System.out.println("13");
        System.out.println(M.dis(s1, s3));
        System.out.println(M.dis(l1, l3));
    }

    @Test
    public void reCheck() {
        System.out.println("测量");
        System.out.println(M.dis(measurePoints[0], measurePoints[1]));
        System.out.println(M.dis(measurePoints[0], measurePoints[2]));

        System.out.println("短边");
        System.out.println(M.dis(measurePointsShort[0], measurePointsShort[1]));
        System.out.println(M.dis(measurePointsShort[0], measurePointsShort[2]));

        System.out.println("长边");
        System.out.println(M.dis(measurePointsLong[0], measurePointsLong[1]));
        System.out.println(M.dis(measurePointsLong[0], measurePointsLong[2]));

        System.out.println("目标");
        System.out.println(M.dis(targetPoints[0], targetPoints[1]));
        System.out.println(M.dis(targetPoints[0], targetPoints[2]));

        AxisInfo r1 = AxisInfo.of(38175186.10, 42676559.46, -15357.32);
        AxisInfo r2 = AxisInfo.of(38174529.88, 42675441.78, -15350.72);
        AxisInfo r3 = AxisInfo.of(38177044.07, 42675173.72, -15359.16);
        AxisInfo r4 = M.get4thPoint(r1, r2, r3);
        System.out.println("12");
        System.out.println(M.dis(r1, r2));
        System.out.println("13");
        System.out.println(M.dis(r1, r3));
        System.out.println("23");
        System.out.println(M.dis(r3, r2));
        System.out.println("34");
        System.out.println(M.dis(r4, r3));
    }

    @Test
    public void checkTwoPlaneJoint() {

        Line line = M.twoPlaneJoint(new Plane(1, 2, 3, 4), new Plane(5, 6, 7, 8));
        System.out.println();
    }

    @Test
    public void checkMeasureAndTarget() {
        Plane tp = M.planeFit(targetPoints);
        Plane mp = M.planeFit(measurePoints);
        U.print(U.f("tp %s", tp.toString()));
        U.print(U.f("mp %s", mp.toString()));
        double angle = M.twoPlaneAngle(tp, mp);
        System.out.println(angle);
        System.out.println(Math.toDegrees(Math.asin(19.1 / 1300)));
    }

    @Test
    public void solveLineFormula() {
        double  a = 15274.3249184036,
                b = 16334.6377959730,
                c = 3910347.7041612230,
                d = -1217342147660.1372;
        double e = -14272.5876091204,
                f = -32556.6940623617,
                g = 3899632.2000378720,
                h = 1993749987653.625;
        Plane p1 = new Plane(15274.3249184036, 16334.6377959730, 3910347.7041612230, -1217342147660.1372);
        Plane p2 = new Plane(-14272.5876091204, -32556.6940623617, 3899632.2000378720, 1993749987653.625);
//        // bh - df / af - be
//        System.out.printf("%.8f%n", (b * h - d * f) / (a * f - b * e));
//        // bg - cf / af - be
//        System.out.printf("%.8f%n", (b * g - c * f) / (a * f - b * e));
//        // de - ah / af - be
//        System.out.printf("%.8f%n", (d * e - a * h) / (a * f - b * e));
//        // ce - ag / af - be
//        System.out.printf("%.8f%n", (c * e - a * g) / (a * f - b * e));
        // (x + a) / d
        // t = x + {(df - bh) / (af - be)} / {(bg - cf) / (af - be)}
        double a1 = (d * f - b * h) / (a * f - b * e);
        double d1 = (b * g - c * f) / (a * f - b * e);
        // (y + b) / e
        // t = y + ((- de + ah) / (af - be)) / ((ce - ag) /  (af - be))
        double b1 = (-d * e + a * h) / (a * f - b * e);
        double e1 = (c * e - a * g) / (a * f - b * e);
        double c1 = 0;
        double f1 = 1;
        System.out.printf("%.8f%n",a1);
        System.out.printf("%.8f%n",d1);
        System.out.printf("%.8f%n",b1);
        System.out.printf("%.8f%n",e1);
    }





    @Test
    public void testFourthPoint() {
        AxisInfo t1 = AxisInfo.of(5.0964, -1.8359, -0.6951);
        AxisInfo t2 = AxisInfo.of(6.9585, -1.634, -0.6934);
        AxisInfo t3 = AxisInfo.of(4.6467, 2.3055, -0.6773);
        AxisInfo t4 = AxisInfo.of(6.5081, 2.5076, -0.6876);

        AxisInfo calcPoint = M.get4thPoint(t1, t2, t3);
        System.out.println(M.sub(calcPoint, t4));

    }

    @Test
    public void testPointOnPlane() {
        AxisInfo
        p1 = AxisInfo.of(-17720.7304271745, -299701.0486066955, -1260.2985824795),
        p2 = AxisInfo.of(157415.6281427322, -2011535.7518863254, -3595.6371978179),
        p3 = AxisInfo.of(-427.8932513637, -468726.3671495668, -1490.8882256306),
        p4 = AxisInfo.of(174708.4653185430, -2180561.0704291966, -3826.2268409691);

        Plane
                pl1 = new Plane(22491.9015857529, -3159.4924206831, 4002707.9799565235, 4496276926.4705130000),
                pl2 = new Plane(96708971.8790116300, 23979666.8071994800, -10324827620.0078740000, -4111880705926.7970000000);
        Stream.of(p1, p2, p3, p4).forEach(elm -> {
            double pl1Diff = M.checkPointIsOnPlane(pl1, elm);
            double pl2Diff = M.checkPointIsOnPlane(pl2, elm);
            System.out.printf("%.8f%n", pl1Diff);
            System.out.printf("%.8f%n", pl2Diff);
        });
    }



    @Test
    public void testPlaneAngle() {
        // 棱镜平面方程
        Plane p1 = new Plane(30472.6500000139, 32588.0000000480, 7801238.8500085880, -2431201666495.292);
        // 千斤顶
        Plane p2 = new Plane(11813.9178381298, 12634.0162240493, 3024456.1859816200, -941552585141.3729000000);
        // 托盘
        Plane p3 = new Plane(15466.0217294904, 16539.6418130789, 3959423.5996609270, -1232620113053.8010000000);

        MeasurePoint point = measurePoints[0];
        System.out.println(M.disBetweenPointAndPlane(point, p1));
        System.out.println(M.disBetweenPointAndPlane(point, p2));
        System.out.println(M.disBetweenPointAndPlane(point, p3));


    }

    @Test
    public void testMTDiff() {

        AxisInfo tc1 = AxisInfo.of(38174262.8211891060, 42677323.4261435050, -16050.9724302381);
        AxisInfo tc2 = AxisInfo.of(38173278.8211891060, 42675730.5261435060, -16067.8724302382);
        AxisInfo tc3 = AxisInfo.of(38177807.5211891100, 42675134.6261435100, -16056.2724302381);
        AxisInfo tc4 = AxisInfo.of(38176823.5211891100, 42673541.7261435100, -16073.1724302381);


        AxisInfo te1 = AxisInfo.of(38174875.7161663800, 42676701.6030004300, -16053.9206335494);
        AxisInfo te2 = AxisInfo.of(38174205.6949759900, 42675517.0184577600, -16066.2625904901);
        AxisInfo te3 = AxisInfo.of(38177290.7840554300, 42675079.4649228700, -16058.6242015118);
        AxisInfo te4 = AxisInfo.of(38176620.7628650400, 42673894.8803802000, -16070.9661584525);


        AxisInfo mc1 = AxisInfo.of(38174264.2101236500, 42677325.6745518740, -16050.9485755697);
        AxisInfo mc2 = AxisInfo.of(38173277.4322545600, 42675728.2777351400, -16067.8962849067);
        AxisInfo mc3 = AxisInfo.of(38177808.9101236500, 42675136.8745518800, -16056.2485755697);
        AxisInfo mc4 = AxisInfo.of(38176822.1322545600, 42673539.4777351400, -16073.1962849067);


        AxisInfo me1 = AxisInfo.of(38172073.0507849450, 42681161.0763587900, -16026.9477250273);
        AxisInfo me2 = AxisInfo.of(38172233.0046196650, 42680051.6639736200, -16035.6244008668);
        AxisInfo me3 = AxisInfo.of(38175636.7525867200, 42676773.2312996400, -16050.5372530343);
        AxisInfo me4 = AxisInfo.of(38175796.7064214400, 42675663.8189144700, -16059.2139288738);

        System.out.println("c1 :" + M.sub(tc1, mc1));
        System.out.println("c2 :" + M.sub(tc2, mc2));
        System.out.println("c3 :" + M.sub(tc3, mc3));
        System.out.println("c4 :" + M.sub(tc4, mc4));


        System.out.println("e1 :" + M.sub(te1, me1));
        System.out.println("e2 :" + M.sub(te2, me2));
        System.out.println("e3 :" + M.sub(te3, me3));
        System.out.println("e4 :" + M.sub(te4, me4));

    }

    @Test
    public void twoPlaneAngleProblem() {
        // [TWEAK]测量平面 Plane{a=15466.0217294904, b=16539.6418130789, c=3959423.5996609270, d=-1232620113053.8010000000}
        Plane mplane = new Plane(15466.0217294904, 16539.6418130789, 3959423.599660927, -1232620113053.801);
        // 垂足
        AxisInfo p0 = AxisInfo.of(38174104.4641125600, 42675424.7615885000, -16067.4033147446);
        // p1
        AxisInfo p1 = AxisInfo.of(38174876.7978768000, 42676703.3540740160, -16053.9020553953);
        // 过 p1 垂直于下平面
        AxisInfo p2 = getLineJointPlane(mplane, p1);
        // AxisInfo.of(38174876.7124949540, 42676703.2627651400, -16075.7604811228)
        double a = M.dis(p0, p1);
        double b = M.dis(p2, p0);
        double rate = b / a;
        // p2 - p0 / x - p0 = b / a
        AxisInfo result = add(divide(sub(p2, p0), rate), p0);
        System.out.println(p2);
        System.out.println(result);
        // AxisInfo.of(38174876.7978768000, 42676703.3540740160, -16053.9020553953);
        // AxisInfo.of(38174104.4641125600, 42675424.7615885000, -16067.4033147446);
        // AxisInfo.of(38174876.7951854540, 42676703.3996639800, -16075.7613759881)

        System.out.println("check");
        AxisInfo normalVec = mplane.getNormalVec();
        AxisInfo direction = sub(p2, p1);
        System.out.println("dot");
        System.out.printf("%.8f", dot(normalVec, direction));

        System.out.println("point dis");
        System.out.println(dis(p2, p1));
        System.out.println("point plane dis");
        System.out.println(disBetweenPointAndPlane(p1, mplane));

        System.out.println("check cos");
        double h = Math.sin(Math.toRadians(0.8384314672)) * a;

        System.out.println();
    }

    @Test
    public void checkReflect() {
        AxisInfo reflect1 = AxisInfo.of(38174876.7951854540, 42676703.3996639800, -16075.7613759876);
        AxisInfo reflect2 = AxisInfo.of(38174204.7587784160, 42675515.5072715100, -16068.1741493112);
        AxisInfo reflect3 = AxisInfo.of(38177291.6981206600, 42675080.9898730740, -16078.4170313349);
        AxisInfo reflect4 = AxisInfo.of(38176619.6617136150, 42673893.0974806100, -16070.8298046587);

        AxisInfo measurePointE1 = AxisInfo.of(38174879.7033307250, 42676707.8911060240, -16075.7914976145);
        AxisInfo measurePointE2 = AxisInfo.of(38174207.2955521200, 42675519.9642534550, -16068.2026763631);
        AxisInfo measurePointE3 = AxisInfo.of(38177294.2970889060, 42675085.2703229400, -16078.4450639018);
        AxisInfo measurePointE4 = AxisInfo.of(38176621.8893103000, 42673897.3434703700, -16070.8562426504);

        System.out.println(dis(reflect1, reflect3));
        System.out.println(dis(reflect1, reflect2));

        double angle = calcVecAngle(sub(reflect1, reflect3), sub(measurePointE1, measurePointE3));
        System.out.println(angle);
    }

    @Test
    public void checkRotateAngle() {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("k1", 20);
//        map.computeIfPresent("k1", (key, val) -> val + 20);
    }

    @Test
    public void testPlane() {
        Plane plane = planeFit(measurePoints[0], measurePoints[1], measurePoints[2]);
        AxisInfo fourthPoint = MeasurePoint.fromMachine(3.5891, -1.2254, -0.7059);

        double v = disBetweenPointAndPlane(fourthPoint, plane);
        System.out.printf("%.8f", v);

    }
}
