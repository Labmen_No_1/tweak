import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.jskj.fine_tune.tweak.*;
import org.junit.Before;
import org.junit.Test;

import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.jskj.fine_tune.tweak.AxisInfo.*;

public class TestMain {
    AxisInfo[] targetPoints = new AxisInfo[3];
    AxisInfo[] measurePoints = new AxisInfo[3];
    AxisInfo[] shortBeforePoints = new AxisInfo[3];
    AxisInfo[] shortAfterPoints = new AxisInfo[3];
    AxisInfo[] longBeforePoints = new AxisInfo[3];
    AxisInfo[] longAfterPoints = new AxisInfo[3];

    AxisInfo[] hb = new AxisInfo[3];
    AxisInfo[] ha = new AxisInfo[3];
    AxisInfo[] rb = new AxisInfo[3];
    AxisInfo[] ra = new AxisInfo[3];

    double a, b;
    double c, d;
    double e, f;
    double g, h;

    Map<String, Double> jackParams = new HashMap<>() {{
        put("R1", 100.);
        put("R2", 100.);
        put("H1", 100.);
        put("V1", 75.);
        put("V2", 75.);
        put("V3", 75.);
        put("V4", 75.);
    }};

    String name;



    private void grp03() {
        name = "test01";
        // 2
        targetPoints = new AxisInfo[] {
                of(7.4994, -0.4274, -0.6773),
                of(6.5272,  1.1686, -0.6914),
                of(3.9411, -2.5938, -0.6697),
        };
        // 1
        targetPoints = new AxisInfo[] {
                of(7.4673, -0.4960, -0.6869),
                of(6.5423,  1.1279, -0.6678),
                of(3.8470, -2.5571, -0.6934),
        };

        measurePoints = new AxisInfo[] {
                of(7.4821,-0.4510,-0.6977),
                of(6.5301,1.1581,-0.6939),
                of(3.8964,-2.5716,-0.6953),
//                of(3.0579, -1.1633, -0.7055),
        };

        shortBeforePoints = new AxisInfo[] {
                of(7.4821,-0.4506,-0.6989),
                of(6.5301,1.1581,-0.6939),
                of(3.8967,-2.5721,-0.6957),
//                of(3.1294, -0.9145, -0.7029),
        };

        shortAfterPoints = new AxisInfo[] {
                of(7.4835,-0.4509,-0.7005),
                of(6.5322,1.1576,-0.6954),
                of(3.8978,-2.5714,-0.6909),
//                of(3.0607, -1.1626, -0.6930),
        };

        longBeforePoints = new AxisInfo[] {
                of(7.4825,-0.4512,-0.6991),
                of(6.5307,1.1575,-0.6939),
                of(3.8968,-2.5716,-0.6954),
//                of(3.0588, -1.1643, -0.7044),
        };

        longAfterPoints = new AxisInfo[] {
                of(7.4818,-0.4500,-0.6948),
                of(6.5294,1.1594,-0.6950),
                of(3.8955,-2.5697,-0.6920),
//                of(3.0537, -1.1565, -0.7077),
        };



        // 短抬前
        a = 0.2385; b = -0.1352;
        // 短抬后
        c = 0.2353; d = -0.2259;
        // 长抬前
        e = 0.2375; f = -0.137;
        // 长抬后
        g = 0.0767; h = -0.1324;
    }

    private void initShift() {
        hb = new AxisInfo[]{
                AxisInfo.of(6.4180, 1.7943, -0.6876),
                AxisInfo.of(4.5608, 1.5769, -0.6851),
                AxisInfo.of(6.9012, -2.3416, -0.6839),
//                AxisInfo.of(5.0447, -2.5598, -0.6953),
        };
        ha = new AxisInfo[]{
                AxisInfo.of(6.4241, 1.7452, -0.6875),
                AxisInfo.of(4.5671, 1.5277, -0.6851),
                AxisInfo.of(6.9068, -2.3908, -0.6841),
//                AxisInfo.of(5.0505, -2.6093, -0.6954),
        };
        rb = new AxisInfo[]{
                AxisInfo.of(6.4179, 1.7945, -0.6877),
                AxisInfo.of(4.5624, 1.5767, -0.6852),
                AxisInfo.of(6.9019, -2.3415, -0.6842),
//                AxisInfo.of(5.0462, -2.5601, -0.6955),
        };
        ra = new AxisInfo[]{
                AxisInfo.of(6.4663, 1.8003, -0.6874),
                AxisInfo.of(4.6101, 1.5828, -0.6848),
                AxisInfo.of(6.9506, -2.3355, -0.6841),
//                AxisInfo.of(5.0934, -2.5541, -0.6952),
        };
    }

    TestData td;
    private void deserial(int num) {
        Gson gson = new Gson();
        String fName = String.format("data/data%d.json", num);
        try (JsonReader fr = new JsonReader(new FileReader(fName))) {
            TestData obj = gson.fromJson(fr, TestData.class);
            td = TestData.copy(obj);

            a = td.getShortBeforeAngle()[0];
            b = td.getShortBeforeAngle()[1];
            c = td.getShortAfterAngle()[0];
            d = td.getShortAfterAngle()[1];

            e = td.getLongBeforeAngle()[0];
            f = td.getLongBeforeAngle()[1];
            g = td.getLongAfterAngle()[0];
            h = td.getLongAfterAngle()[1];

            targetPoints = td.getTarget();
            measurePoints = td.getMeasure();
            shortBeforePoints = td.getShortBefore();
            shortAfterPoints = td.getShortAfter();
            longBeforePoints = td.getLongBefore();
            longAfterPoints = td.getLongAfter();

            hb = td.getvShiftBefore();
            ha = td.getvShiftAfter();

            rb = td.gethShiftBefore();
            ra = td.gethShiftAfter();
            name = U.f("task %d", num);

        } catch (Exception ex) {}
    }

    @Before
    public void setup() {
        deserial(16);
        // grp03();
    }

    @Test
    public void go() {
        Main main = new Main(name)
                .setTargetGroup(targetPoints)
                .setTolerate(1);

        main.setVShiftGroupBefore(hb);
        main.setVShiftGroupAfter(ha);

        main.setHShiftGroupBefore(rb);
        main.setHShiftGroupAfter(ra);

        // 抬升长短边
        main.setShortGroupBefore(a, b, shortBeforePoints);
        main.setShortGroupAfter(c, d, shortAfterPoints);

        main.setLongGroupBefore(e, f, longBeforePoints);
        main.setLongGroupAfter(g, h, longAfterPoints);

        // loop
        // measure
        TweakResult resp = main
                .saveJackInfo(jackParams)
                .saveMeasurePoints(measurePoints)
                .fit();

        List<AxisInfo> show = main.show();
    }


}
