import com.jskj.fine_tune.tweak.AxisInfo;
/*
* grep -i "private" src/test/java/TestData.java | perl -pe 's/^.*?([\w]+);$/$1/gi;s/\n/ /g'
*
* */
public class TestData {
    private AxisInfo[] target;
    private AxisInfo[] measure;
    private AxisInfo[] shortBefore;
    private AxisInfo[] shortAfter;
    private AxisInfo[] longBefore;
    private AxisInfo[] longAfter;

    private double[] shortBeforeAngle;
    private double[] shortAfterAngle;
    private double[] longBeforeAngle;
    private double[] longAfterAngle;

    private AxisInfo[] vShiftBefore;
    private AxisInfo[] vShiftAfter;
    private AxisInfo[] hShiftBefore;
    private AxisInfo[] hShiftAfter;

    private String taskName;

    public AxisInfo[] getTarget() {
        return target;
    }

    public void setTarget(AxisInfo[] target) {
        this.target = convert(target);
    }

    public AxisInfo[] getMeasure() {
        return measure;
    }

    public void setMeasure(AxisInfo[] measure) {
        this.measure = convert(measure);
    }

    public AxisInfo[] getShortBefore() {
        return shortBefore;
    }

    public void setShortBefore(AxisInfo[] shortBefore) {
        this.shortBefore = convert(shortBefore);
    }

    public AxisInfo[] getShortAfter() {
        return shortAfter;
    }

    public void setShortAfter(AxisInfo[] shortAfter) {
        this.shortAfter = convert(shortAfter);
    }

    public AxisInfo[] getLongBefore() {
        return longBefore;
    }

    public void setLongBefore(AxisInfo[] longBefore) {
        this.longBefore = convert(longBefore);
    }

    public AxisInfo[] getLongAfter() {
        return longAfter;
    }

    public void setLongAfter(AxisInfo[] longAfter) {
        this.longAfter = convert(longAfter);
    }

    public double[] getShortBeforeAngle() {
        return shortBeforeAngle;
    }

    public void setShortBeforeAngle(double[] shortBeforeAngle) {
        this.shortBeforeAngle = shortBeforeAngle;
    }

    public double[] getShortAfterAngle() {
        return shortAfterAngle;
    }

    public void setShortAfterAngle(double[] shortAfterAngle) {
        this.shortAfterAngle = shortAfterAngle;
    }

    public double[] getLongBeforeAngle() {
        return longBeforeAngle;
    }

    public void setLongBeforeAngle(double[] longBeforeAngle) {
        this.longBeforeAngle = longBeforeAngle;
    }

    public double[] getLongAfterAngle() {
        return longAfterAngle;
    }

    public void setLongAfterAngle(double[] longAfterAngle) {
        this.longAfterAngle = longAfterAngle;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public AxisInfo[] getvShiftBefore() {
        return vShiftBefore;
    }

    public void setvShiftBefore(AxisInfo[] vShiftBefore) {
        this.vShiftBefore = convert(vShiftBefore);
    }

    public AxisInfo[] getvShiftAfter() {
        return vShiftAfter;
    }

    public void setvShiftAfter(AxisInfo[] vShiftAfter) {
        this.vShiftAfter = convert(vShiftAfter);
    }

    public AxisInfo[] gethShiftBefore() {
        return hShiftBefore;
    }

    public void sethShiftBefore(AxisInfo[] hShiftBefore) {
        this.hShiftBefore = convert(hShiftBefore);
    }

    public AxisInfo[] gethShiftAfter() {
        return hShiftAfter;
    }

    public void sethShiftAfter(AxisInfo[] hShiftAfter) {
        this.hShiftAfter = convert(hShiftAfter);
    }

    private AxisInfo[] convert(AxisInfo[] arr) {
        AxisInfo[] ret = new AxisInfo[arr.length];
        for (int i = 0; i < arr.length; ++i) {
            ret[i] = AxisInfo.of(arr[i]);
        }
        return ret;
    }

    public static TestData copy(TestData td) {
        TestData ret = new TestData();
        ret.setMeasure(td.getMeasure());
        ret.setTarget(td.getTarget());

        ret.setShortBefore(td.getShortBefore());
        ret.setShortAfter(td.getShortAfter());
        ret.setLongBefore(td.getLongBefore());
        ret.setLongAfter(td.getLongAfter());

        ret.setShortBeforeAngle(td.getShortBeforeAngle());
        ret.setShortAfterAngle(td.getShortAfterAngle());
        ret.setLongBeforeAngle(td.getLongBeforeAngle());
        ret.setLongAfterAngle(td.getLongAfterAngle());

        ret.setvShiftBefore(td.getvShiftBefore());
        ret.setvShiftAfter(td.getvShiftAfter());
        ret.sethShiftBefore(td.gethShiftBefore());
        ret.sethShiftAfter(td.gethShiftAfter());
        return ret;
    }
}
